#include "axi.h"
#include "xildma.h"

#include "dmachannels.h"
#include "usbep0.h"

#include <cyu3error.h>
#include <cyu3mmu.h>
#include <cyu3usb.h>
#include <cyu3utils.h>

#define XIL_IO_H
#include <xil_types.h>
#include <xspi.h>
#include <xstatus.h>
#undef XSpi_ReadReg
#undef XSpi_WriteReg

#include <csetjmp>

XSpi g_XSpi0;
static jmp_buf s_JmpBuf;
static uint32_t s_DataValidForSlave = 0U;

extern volatile int g_AXILiteResultRd;
extern volatile int g_AXILiteResultWr;

/* extern */
u32 XSpi_ReadReg(u32 BaseAddress, u32 RegOffset) {
    uint32_t data;

    g_AXILiteResultRd = AXILiteRead(BaseAddress + RegOffset, data);
    if (g_AXILiteResultRd == 0)
        return data;
    else
        std::longjmp(s_JmpBuf, g_AXILiteResultRd);
}

/* extern */
void XSpi_WriteReg(u32 BaseAddress, u32 RegOffset, u32 RegisterValue) {
    g_AXILiteResultWr = AXILiteWrite(BaseAddress + RegOffset, RegisterValue);
    if (g_AXILiteResultWr == 0)
        return;
    else
        std::longjmp(s_JmpBuf, g_AXILiteResultWr);
}

static auto GetSpiInstance(uint16_t wIndex) -> XSpi* {
    if (wIndex == 0U)
        return &g_XSpi0;
    else
        return nullptr;
}

void HandleXilSpiInitRqt(uint16_t wIndex) {
    XSpi* instance = nullptr;
    int ret = XST_SUCCESS;

    instance = GetSpiInstance(wIndex);
    if (instance != nullptr) {
        if (setjmp(s_JmpBuf) == 0)
            do {
                ret = XSpi_Initialize(instance, wIndex);
                if (ret != XST_SUCCESS)
                    break;

                ret = XSpi_SetOptions(instance, XSP_MASTER_OPTION);
                if (ret != XST_SUCCESS)
                    break;

                CyU3PUsbAckSetup();
                return;
            } while (0);
    }

    CyU3PUsbStall(0, CyTrue, CyFalse);
}

void HandleXilSpiResetRqt(uint16_t wIndex) {
    XSpi* instance = nullptr;

    instance = GetSpiInstance(wIndex);
    if (instance != nullptr) {
        if (setjmp(s_JmpBuf) == 0) {
            XSpi_Reset(instance);

            CyU3PUsbAckSetup();
            return;
        }
    }

    CyU3PUsbStall(0, CyTrue, CyFalse);
}

void HandleXilSpiSelfTestRqt(uint16_t wIndex) {
    XSpi* instance = nullptr;
    int ret = XST_SUCCESS;

    instance = GetSpiInstance(wIndex);
    if (instance != nullptr) {
        if (setjmp(s_JmpBuf) == 0) {
            ret = XSpi_SelfTest(instance);
            if (ret == XST_SUCCESS) {
                CyU3PUsbAckSetup();
                return;
            }
        }
    }

    CyU3PUsbStall(0, CyTrue, CyFalse);
}

void HandleXilSpiStartStopRqt(uint16_t wValue, uint16_t wIndex) {
    XSpi* instance = nullptr;
    int ret = XST_SUCCESS;

    instance = GetSpiInstance(wIndex);
    if (instance != nullptr) {
        if (setjmp(s_JmpBuf) == 0)
            do {
                if (wValue != 0U) {
                    ret = XSpi_Start(instance);
                    if (ret != XST_SUCCESS)
                        break;

                    XSpi_IntrGlobalDisable(instance);
                } else {
                    ret = XSpi_Stop(instance);
                    if (ret != XST_SUCCESS)
                        break;
                }

                CyU3PUsbAckSetup();
                return;
            } while (0);
    }

    CyU3PUsbStall(0, CyTrue, CyFalse);
}

void HandleXilSpiStatsRqt(uint8_t bReqType, uint16_t wValue, uint16_t wIndex, uint16_t wLength) {
    XSpi* instance = GetSpiInstance(wIndex);
    if (instance != nullptr) {
        if ((bReqType & 0x80) != 0 && wLength == sizeof(XSpi_Stats)) {
            XSpi_GetStats(instance, (XSpi_Stats*)g_Ep0Buffer);

            CyU3PUsbSendEP0Data(wLength, g_Ep0Buffer);
            return;
        } else if ((bReqType & 0x80) == 0 && wLength == 0) {
            XSpi_ClearStats(instance);

            CyU3PUsbAckSetup();
            return;
        }
    }

    CyU3PUsbStall(0, CyTrue, CyFalse);
}

void HandleXilSpiTransferRqt(uint8_t bReqType, uint16_t wValue, uint16_t wIndex, uint16_t wLength) {
    if (wLength == 0 || wLength > sizeof(g_Ep0Buffer) / 2) {
        CyU3PUsbStall(0, CyTrue, CyFalse);
        return;
    }

    XSpi* instance = GetSpiInstance(wIndex);
    if (instance == nullptr) {
        CyU3PUsbStall(0, CyTrue, CyFalse);
        return;
    }

    int ret = XST_SUCCESS;

    if ((bReqType & 0x80) == 0) {
        uint16_t len;
        auto ret = CyU3PUsbGetEP0Data(wLength, &g_Ep0Buffer[0], &len);
        if (ret == CY_U3P_SUCCESS && len == wLength) {
            if (setjmp(s_JmpBuf) == 0) {
                do {
                    ret = XSpi_SetSlaveSelect(instance, 1U << wValue);
                    if (ret != XST_SUCCESS)
                        break;

                    ret = XSpi_Transfer(instance, &g_Ep0Buffer[0],
                                        &g_Ep0Buffer[sizeof g_Ep0Buffer / 2], len);
                    if (ret != XST_SUCCESS)
                        break;

                    s_DataValidForSlave = wValue << 16 | len;
                } while (0);

                ret = XSpi_SetSlaveSelect(instance, 0U);
            } else {
                s_DataValidForSlave = 0U;
            }
            return;
        }
    } else {
        if ((s_DataValidForSlave & 0xffff) == wLength && (s_DataValidForSlave >> 16) == wValue) {
            auto status = CyU3PUsbSendEP0Data(wLength, &g_Ep0Buffer[sizeof g_Ep0Buffer / 2]);
            if (status == CY_U3P_SUCCESS) {
                s_DataValidForSlave = 0U;
                return;
            }
        }
    }

    CyU3PUsbStall(0, CyTrue, CyFalse);
}
