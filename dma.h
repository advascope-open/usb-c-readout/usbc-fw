#pragma once
#include <cyu3dma.h>

void DmaSlFifoStart();
void DmaSlFifoStop();
void DmaUsbStart();
void DmaUsbStop();
void DmaI2cReset();
void DmaI2cStart();
void DmaI2cStop();
