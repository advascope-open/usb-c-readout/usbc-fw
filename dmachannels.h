#pragma once
#include <cyu3dma.h>

extern CyU3PDmaChannel g_DmaHandleSlFifoCtoP;
extern CyU3PDmaChannel g_DmaHandleSlFifoPtoC;
extern CyU3PDmaChannel g_DmaHandleSlFifoUtoC;
extern CyU3PDmaMultiChannel g_DmaHandleSlFifoPtoU;
extern CyU3PDmaChannel g_DmaHandleI2cRx;
extern CyU3PDmaChannel g_DmaHandleI2cTx;
