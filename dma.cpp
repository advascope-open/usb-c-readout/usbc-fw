#include "dma.h"
#include "dmasockets.h"
#include "usbendpoints.h"

#include "appln.h"
#include "dscr.h"
#include "usb.h"

#include <cyu3error.h>
#include <cyu3utils.h>

#define CHECK_SUCCESS(f)                        \
    do {                                        \
        CyU3PReturnStatus_t apiRetStatus = (f); \
        if (apiRetStatus != CY_U3P_SUCCESS) {   \
            goto handle_fatal_error;            \
        }                                       \
    } while (0)

void DmaUsbToCpuCallback(CyU3PDmaChannel*, CyU3PDmaCbType_t, CyU3PDmaCBInput_t*);
void DmaPportToUsbCallback(CyU3PDmaMultiChannel*, CyU3PDmaCbType_t, CyU3PDmaCBInput_t*);

CyU3PDmaChannel g_DmaHandleSlFifoCtoP;
CyU3PDmaChannel g_DmaHandleSlFifoPtoC;
CyU3PDmaChannel g_DmaHandleSlFifoUtoC;
CyU3PDmaMultiChannel g_DmaHandleSlFifoPtoU;
CyU3PDmaChannel g_DmaHandleI2cRx;
CyU3PDmaChannel g_DmaHandleI2cTx;

extern uint8_t g_SlFifoBuffer[32];

static CyU3PDmaChannelConfig_t s_DmaSlFifoCtoPConfig = {
    .size = 4128,
    .count = 1,
    .prodSckId = CY_FX_PRODUCER_CPU_SOCKET,
    .consSckId = CY_FX_CONSUMER_PPORT_SOCKET,
    .prodAvailCount = 0,
    .prodHeader = 0,
    .prodFooter = 0,
    .consHeader = 0,
    .dmaMode = CY_U3P_DMA_MODE_BYTE,
    .notification = 0,
    .cb = nullptr,
};

static CyU3PDmaChannelConfig_t s_DmaSlFifoPtoCConfig = {
    .size = sizeof(g_SlFifoBuffer),
    .count = 0,
    .prodSckId = CY_FX_PRODUCER_PPORT_SOCKET,
    .consSckId = CY_FX_CONSUMER_CPU_SOCKET,
    .prodAvailCount = 0,
    .prodHeader = 0,
    .prodFooter = 0,
    .consHeader = 0,
    .dmaMode = CY_U3P_DMA_MODE_BYTE,
    .notification = 0,
    .cb = nullptr,
};

static CyU3PDmaChannelConfig_t s_DmaSlFifoUtoCConfig = {
    .size = 8192,
    .count = 1,
    .prodSckId = CY_FX_PRODUCER_USB_SOCKET,
    .consSckId = CY_FX_CONSUMER_CPU_SOCKET,
    .prodAvailCount = 0,
    .prodHeader = 0,
    .prodFooter = 0,
    .consHeader = 0,
    .dmaMode = CY_U3P_DMA_MODE_BYTE,
    .notification = CY_U3P_DMA_CB_PROD_EVENT,
    .cb = &DmaUsbToCpuCallback,
};

static CyU3PDmaMultiChannelConfig_t s_DmaSlFifoPtoUConfig = {
    .size = 24576,
    .count = 2,
    .validSckCount = 2,
    .prodSckId = {[0] = CY_FX_PRODUCER_DUAL_PPORT_SOCKET_FIRST,
                  [1] = CY_FX_PRODUCER_DUAL_PPORT_SOCKET_SECOND},
    .consSckId = {[0] = CY_FX_CONSUMER_USB_SOCKET},
    .prodAvailCount = 0,
    .prodHeader = 0,
    .prodFooter = 0,
    .consHeader = 0,
    .dmaMode = CY_U3P_DMA_MODE_BYTE,
    .notification = CY_U3P_DMA_CB_CONS_SUSP | CY_U3P_DMA_CB_PROD_EVENT,
    .cb = &DmaPportToUsbCallback,
};

static CyU3PDmaChannelConfig_t s_I2cRxConfig = {
    .size = 128,
    .count = 0,
    .prodSckId = CY_FX_PRODUCER_I2C_SOCKET,
    .consSckId = CY_FX_CONSUMER_CPU_SOCKET,
    .prodAvailCount = 0,
    .prodHeader = 0,
    .prodFooter = 0,
    .consHeader = 0,
    .dmaMode = CY_U3P_DMA_MODE_BYTE,
    .notification = 0,
    .cb = nullptr,
};

static CyU3PDmaChannelConfig_t s_I2cTxConfig = {
    .size = 128,
    .count = 0,
    .prodSckId = CY_FX_PRODUCER_CPU_SOCKET,
    .consSckId = CY_FX_CONSUMER_I2C_SOCKET,
    .prodAvailCount = 0,
    .prodHeader = 0,
    .prodFooter = 0,
    .consHeader = 0,
    .dmaMode = CY_U3P_DMA_MODE_BYTE,
    .notification = 0,
    .cb = nullptr,
};

void DmaSlFifoStart() {
    /* CPU to p-port */
    CHECK_SUCCESS(CyU3PDmaChannelCreate(&g_DmaHandleSlFifoCtoP, CY_U3P_DMA_TYPE_MANUAL_OUT,
                                        &s_DmaSlFifoCtoPConfig));

    CHECK_SUCCESS(CyU3PDmaChannelSetXfer(&g_DmaHandleSlFifoCtoP, 0U));

    /* P-port to CPU */
    CHECK_SUCCESS(CyU3PDmaChannelCreate(&g_DmaHandleSlFifoPtoC, CY_U3P_DMA_TYPE_MANUAL_IN,
                                        &s_DmaSlFifoPtoCConfig));

    return;

handle_fatal_error:
    /* Cannot recover from this error. */
    while (1)
        ;
}

void DmaSlFifoStop() {
    CHECK_SUCCESS(CyU3PDmaChannelDestroy(&g_DmaHandleSlFifoPtoC));
    CHECK_SUCCESS(CyU3PDmaChannelDestroy(&g_DmaHandleSlFifoCtoP));

    return;

handle_fatal_error:
    /* Cannot recover from this error. */
    while (1)
        ;
}

void DmaI2cStart() {
    /* CPU to I2C */
    CHECK_SUCCESS(
        CyU3PDmaChannelCreate(&g_DmaHandleI2cTx, CY_U3P_DMA_TYPE_MANUAL_OUT, &s_I2cTxConfig));

    /* I2C to CPU */
    CHECK_SUCCESS(
        CyU3PDmaChannelCreate(&g_DmaHandleI2cRx, CY_U3P_DMA_TYPE_MANUAL_IN, &s_I2cRxConfig));

    return;

handle_fatal_error:
    /* Cannot recover from this error. */
    while (1)
        ;
}

void DmaI2cStop() {
    CHECK_SUCCESS(CyU3PDmaChannelDestroy(&g_DmaHandleI2cRx));
    CHECK_SUCCESS(CyU3PDmaChannelDestroy(&g_DmaHandleI2cTx));

    return;

handle_fatal_error:
    /* Cannot recover from this error. */
    while (1)
        ;
}

void DmaI2cReset() {
    CHECK_SUCCESS(CyU3PDmaChannelReset(&g_DmaHandleI2cRx));
    CHECK_SUCCESS(CyU3PDmaChannelReset(&g_DmaHandleI2cTx));

    return;

handle_fatal_error:
    /* Cannot recover from this error. */
    while (1)
        ;
}

void DmaUsbStart() {
    auto const usbSpeed = CyU3PUsbGetSpeed();

    /* USB to CPU */
    s_DmaSlFifoUtoCConfig.size = UsbBulkEpPacketSize(usbSpeed);
    if (usbSpeed == CY_U3P_SUPER_SPEED)
        s_DmaSlFifoUtoCConfig.size *= CY_FX_EP_PRODUCER_BURST_LENGTH * 2;

    CHECK_SUCCESS(CyU3PDmaChannelCreate(&g_DmaHandleSlFifoUtoC, CY_U3P_DMA_TYPE_MANUAL_IN,
                                        &s_DmaSlFifoUtoCConfig));

    CHECK_SUCCESS(CyU3PDmaChannelSetXfer(&g_DmaHandleSlFifoUtoC, 0U));

    /* P-port to USB */
    s_DmaSlFifoPtoUConfig.size = UsbBulkEpPacketSize(usbSpeed);
    if (usbSpeed == CY_U3P_SUPER_SPEED)
        s_DmaSlFifoPtoUConfig.size *= CY_FX_EP_CONSUMER_BURST_LENGTH * 2;

    CHECK_SUCCESS(CyU3PDmaMultiChannelCreate(
        &g_DmaHandleSlFifoPtoU, CY_U3P_DMA_TYPE_AUTO_MANY_TO_ONE, &s_DmaSlFifoPtoUConfig));

    CHECK_SUCCESS(CyU3PDmaMultiChannelSetSuspend(&g_DmaHandleSlFifoPtoU, CY_U3P_DMA_SCK_SUSP_NONE,
                                                 CY_U3P_DMA_SCK_SUSP_EOP));

    CHECK_SUCCESS(CyU3PDmaMultiChannelSetXfer(&g_DmaHandleSlFifoPtoU, 0U, 0U));

    return;

handle_fatal_error:
    /* Cannot recover from this error. */
    while (1)
        ;
}

void DmaUsbStop() {
    CHECK_SUCCESS(CyU3PDmaChannelDestroy(&g_DmaHandleSlFifoUtoC));
    CHECK_SUCCESS(CyU3PDmaMultiChannelDestroy(&g_DmaHandleSlFifoPtoU));

    return;

handle_fatal_error:
    /* Cannot recover from this error. */
    while (1)
        ;
}

void DmaUsbToCpuCallback(
    CyU3PDmaChannel* handle, /**< Handle to the DMA channel. */
    CyU3PDmaCbType_t type,   /**< The type of callback notification being generated. */
    CyU3PDmaCBInput_t* input /**< Union that contains data related to the notification.
                                  The input parameter will be a pointer to a CyU3PDmaBuffer_t
                                  variable in the cases where the callback type is
                                  CY_U3P_DMA_CB_RECV_CPLT or CY_U3P_DMA_CB_PROD_EVENT. */
) {
    if (type == CY_U3P_DMA_CB_PROD_EVENT) {
        CyU3PEventSet(&g_ApplnEvent, CY_FX_USB_OUTI_TASK, CYU3P_EVENT_OR);
    }
}

void DmaPportToUsbCallback(
    CyU3PDmaMultiChannel* handle, /**< Handle to the multi-socket DMA channel. */
    CyU3PDmaCbType_t type,        /**< The callback type. */
    CyU3PDmaCBInput_t* input      /**< Pointer to a union that contains the callback related data.
                                       Will point to a valid CyU3PDmaBuffer_t variable if the callback
                                       type is CY_U3P_DMA_CB_RECV_CPLT or CY_U3P_DMA_CB_PROD_EVENT. */
) {
    if (type == CY_U3P_DMA_CB_CONS_SUSP) {
        CyU3PDmaMultiChannelResume(handle, CyFalse, CyTrue);
    }
}
