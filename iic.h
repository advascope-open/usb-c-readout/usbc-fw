#pragma once
#include <cyu3i2c.h>

constexpr uint32_t CY_FX_I2C_BITRATE = 400000;

constexpr uint8_t CY_FX_RQT_I2C_TRANSFER = 0xE0;
constexpr uint8_t CY_FX_RQT_I2C_TRANSFER_RESULT = 0xE1;

void I2cInit();

void HandleI2cTransferRqt(uint8_t bReqType, uint16_t wValue, uint16_t wIndex, uint16_t wLength);
void HandleI2cTransferResultRqt(uint8_t bReqType,
                                uint16_t wValue,
                                uint16_t wIndex,
                                uint16_t wLength);
