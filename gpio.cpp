#include "gpio.h"
#include "usbep0.h"

#include <cyu3error.h>
#include <cyu3gpio.h>
#include <cyu3usb.h>

#define CHECK_SUCCESS(f)                        \
    do {                                        \
        CyU3PReturnStatus_t apiRetStatus = (f); \
        if (apiRetStatus != CY_U3P_SUCCESS) {   \
            goto handle_fatal_error;            \
        }                                       \
    } while (0)

void GpioInterrupt(uint8_t);

static const CyU3PGpioClock_t s_GpioClock = {
    .fastClkDiv = 2,
    .slowClkDiv = 0,
    .halfDiv = 0,
    .simpleDiv = CY_U3P_GPIO_SIMPLE_DIV_BY_2,
    .clkSrc = CY_U3P_SYS_CLK,
};

static const CyU3PGpioSimpleConfig_t s_BiasEn = {
    .outValue = CyFalse,
    .driveLowEn = CyTrue,
    .driveHighEn = CyTrue,
    .inputEn = CyFalse,
    .intrMode = CY_U3P_GPIO_NO_INTR,
};

static const CyU3PGpioSimpleConfig_t s_Cdce6214Pdn = {
    .outValue = CyFalse,
    .driveLowEn = CyTrue,
    .driveHighEn = CyTrue,
    .inputEn = CyFalse,
    .intrMode = CY_U3P_GPIO_NO_INTR,
};

static const CyU3PGpioSimpleConfig_t s_FpgaPwrEn = {
    .outValue = CyFalse,
    .driveLowEn = CyTrue,
    .driveHighEn = CyTrue,
    .inputEn = CyFalse,
    .intrMode = CY_U3P_GPIO_NO_INTR,
};

static const CyU3PGpioSimpleConfig_t s_FpgaPwrInt = {
    .outValue = CyFalse,
    .driveLowEn = CyFalse,
    .driveHighEn = CyFalse,
    .inputEn = CyTrue,
    .intrMode = CY_U3P_GPIO_INTR_NEG_EDGE,
};

static const CyU3PGpioSimpleConfig_t s_DdrPwrEn = {
    .outValue = CyTrue,
    .driveLowEn = CyTrue,
    .driveHighEn = CyTrue,
    .inputEn = CyFalse,
    .intrMode = CY_U3P_GPIO_NO_INTR,
};

void GpioInit() {
    CHECK_SUCCESS(CyU3PGpioInit(const_cast<CyU3PGpioClock_t*>(&s_GpioClock), &GpioInterrupt));

    // BIAS EN
    CHECK_SUCCESS(CyU3PGpioSetSimpleConfig(CY_FX_GPIO_BIAS_EN,
                                           const_cast<CyU3PGpioSimpleConfig_t*>(&s_BiasEn)));

    // CDCE6214 PDN
    CHECK_SUCCESS(CyU3PGpioSetSimpleConfig(CY_FX_GPIO_CDCE6214_PDN,
                                           const_cast<CyU3PGpioSimpleConfig_t*>(&s_Cdce6214Pdn)));

    // LP8758-E3 nRST
    CHECK_SUCCESS(CyU3PGpioSetSimpleConfig(CY_FX_GPIO_FPGA_PWR_EN,
                                           const_cast<CyU3PGpioSimpleConfig_t*>(&s_FpgaPwrEn)));

    // LP8758-E3 nINT
    CHECK_SUCCESS(CyU3PGpioSetSimpleConfig(CY_FX_GPIO_FPGA_PWR_INT,
                                           const_cast<CyU3PGpioSimpleConfig_t*>(&s_FpgaPwrInt)));

    // DDR PWR EN
    CHECK_SUCCESS(CyU3PGpioSetSimpleConfig(CY_FX_GPIO_DDR_PWR_EN,
                                           const_cast<CyU3PGpioSimpleConfig_t*>(&s_DdrPwrEn)));

    return;

handle_fatal_error:
    /* Cannot recover from this error. */
    while (1)
        ;
}

void HandleGpioGetValue(uint8_t bReqType, uint16_t wValue, uint16_t wIndex, uint16_t wLength) {
    if (wLength != sizeof(CyBool_t) || wIndex > 0xff || wValue != 0 || (bReqType & 0x80) == 0) {
        CyU3PUsbStall(0, CyTrue, CyFalse);
        return;
    }

    auto* pdwEp0Buffer = reinterpret_cast<uint32_t*>(g_Ep0Buffer);

    CyBool_t state;
    auto r = CyU3PGpioSimpleGetValue(wIndex & 0xff, &state);
    if (r == CY_U3P_SUCCESS) {
        *pdwEp0Buffer = state != CyFalse ? 1U : 0U;
    } else {
        *pdwEp0Buffer = static_cast<uint32_t>(-r);
    }

    CyU3PUsbSendEP0Data(sizeof(uint32_t), g_Ep0Buffer);
}

void HandleGpioSetValue(uint8_t bReqType, uint16_t wValue, uint16_t wIndex, uint16_t wLength) {
    if (wLength != sizeof(CyBool_t) || wIndex > 0xff || (bReqType & 0x80) == 0) {
        CyU3PUsbStall(0, CyTrue, CyFalse);
        return;
    }

    auto* pdwEp0Buffer = reinterpret_cast<uint32_t*>(g_Ep0Buffer);

    auto r = CyU3PGpioSimpleSetValue(wIndex & 0xff, wValue != 0U ? CyTrue : CyFalse);
    if (r == CY_U3P_SUCCESS) {
        *pdwEp0Buffer = 0U;
    } else {
        *pdwEp0Buffer = static_cast<uint32_t>(-r);
    }

    CyU3PUsbSendEP0Data(sizeof(uint32_t), g_Ep0Buffer);
}

void GpioInterrupt(uint8_t gpioId /**< Indicates the GPIO pin id that triggered the interrupt. */) {
    (void)gpioId;
}
