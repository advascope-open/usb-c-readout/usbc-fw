#include "versionrqt.h"
#include "usbep0.h"

#include <cyu3error.h>
#include <cyu3system.h>
#include <cyu3usb.h>
#include <cyu3utils.h>

constexpr uint16_t CY_FX_VERSION_API_RQR = 0x0100;
constexpr uint16_t CY_FX_VERSION_THREADX_RQR = 0x0101;

void HandleVersionRqt(uint8_t bReqType, uint16_t wValue, uint16_t wIndex, uint16_t wLength) {
    CyBool_t isHandled = CyFalse;
    CyU3PReturnStatus_t status = CY_U3P_SUCCESS;

    if ((bReqType & 0x80) != 0) {
        switch (wIndex) {
            case CY_FX_VERSION_API_RQR: {
                auto const numVer = ((wLength + 1U) & ~1U) >> 1;
                auto* pVer = reinterpret_cast<uint16_t*>(g_Ep0Buffer);

                status = CyU3PSysGetApiVersion(&pVer[0], &pVer[1], &pVer[2], &pVer[3]);
                if (status != CY_U3P_SUCCESS) {
                    CyU3PUsbStall(0, CyTrue, CyFalse);
                    break;
                }

                if ((0U < numVer && numVer <= 4U) && (wLength & 0x1) == 0U) {
                    CyU3PUsbSendEP0Data(wLength, g_Ep0Buffer);
                } else {
                    CyU3PUsbStall(0, CyTrue, CyFalse);
                }
            } break;

            case CY_FX_VERSION_THREADX_RQR: {
                uint16_t const wLengthMax = strlen(_tx_version_id) + 1;
                CyU3PUsbSendEP0Data(CY_U3P_MIN(wLength, wLengthMax), (uint8_t*)_tx_version_id);
            } break;

            default:
                CyU3PUsbStall(0, CyTrue, CyFalse);
                break;
        }
    } else {
        CyU3PUsbStall(0, CyTrue, CyFalse);
    }
}
