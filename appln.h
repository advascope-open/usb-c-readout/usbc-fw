#pragma once
#include <cyu3system.h>
#include <cyu3types.h>

void ApplnStart();
void ApplnStop();

extern CyU3PEvent g_ApplnEvent;
extern CyU3PThread g_ApplnThread;
extern volatile CyBool_t g_IsApplnActive;

extern volatile uint32_t g_setupdat0;
extern volatile uint32_t g_setupdat1;

constexpr uint32_t CY_FX_USB_EP0_TASK = (1U << 0);
constexpr uint32_t CY_FX_USB_OUTI_TASK = (1U << 1);
