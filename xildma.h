#pragma once

#include <cyu3types.h>

constexpr uint8_t CY_FX_RQT_XAXIDMA_INITIALIZE = 0xC0;
constexpr uint8_t CY_FX_RQT_XAXIDMA_RESET = 0xC1;
constexpr uint8_t CY_FX_RQT_XAXIDMA_MM2S = 0xC2;
constexpr uint8_t CY_FX_RQT_XAXIDMA_S2MM = 0xC3;

typedef struct XAxiDma XAxiDma;
extern XAxiDma g_XAxiDma0;
extern XAxiDma g_XAxiDma1;

void HandleXilDmaInitRqt(uint16_t wIndex);
void HandleXilDmaResetRqt(uint16_t wIndex);
void HandleXilDmaToStreamRqt(uint8_t bReqType, uint16_t wValue, uint16_t wIndex, uint16_t wLength);
void HandleXilDmaToMemoryRqt(uint8_t bReqType, uint16_t wValue, uint16_t wIndex, uint16_t wLength);
