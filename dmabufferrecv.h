#pragma once

#include <cyu3dma.h>
#include <cyu3error.h>

class DmaBufferRecv {
public:
    DmaBufferRecv(CyU3PDmaChannel& ch) : dma_(ch), dmaInfo_(), resetOnCleanup_(false) {}

    ~DmaBufferRecv() {
        if (resetOnCleanup_)
            Reset();
    }

    auto Setup(uint8_t* buf, uint16_t len) -> CyU3PReturnStatus_t {
        dmaInfo_ = {
            .buffer = buf,
            .count = 0,
            .size = len,
            .status = 0,
        };

        auto ret = CyU3PDmaChannelSetupRecvBuffer(&dma_, &dmaInfo_);
        if (ret == CY_U3P_SUCCESS)
            resetOnCleanup_ = true;

        return ret;
    }

    auto WaitFor(uint32_t waitOption) -> CyU3PReturnStatus_t {
        auto ret = CyU3PDmaChannelWaitForRecvBuffer(&dma_, &dmaInfo_, waitOption);
        if (ret == CY_U3P_SUCCESS)
            resetOnCleanup_ = false;

        return ret;
    }

    auto Reset() -> CyU3PReturnStatus_t { return CyU3PDmaChannelReset(&dma_); }

    auto Data() -> uint32_t* { return reinterpret_cast<uint32_t*>(dmaInfo_.buffer); }
    auto Count() -> uint16_t { return dmaInfo_.count; }
    auto Capacity() -> uint16_t { return dmaInfo_.size; }

private:
    CyU3PDmaChannel& dma_;
    CyU3PDmaBuffer_t dmaInfo_;
    bool resetOnCleanup_;
};
