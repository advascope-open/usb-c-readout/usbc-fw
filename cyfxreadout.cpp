#include "gpio.h"

#include <cyu3error.h>
#include <cyu3system.h>
#include <cyu3types.h>

#define CY_FX_GPIO_TO_LOFLAG(gpio) (1 << (gpio))
#define CY_FX_GPIO_TO_HIFLAG(gpio) (1 << ((gpio)-32))

#define CHECK_SUCCESS(f)                        \
    do {                                        \
        CyU3PReturnStatus_t apiRetStatus = (f); \
        if (apiRetStatus != CY_U3P_SUCCESS) {   \
            goto handle_fatal_error;            \
        }                                       \
    } while (0)

static const CyU3PSysClockConfig_t s_SysClockConfig = {
    .setSysClk400 = CyTrue,
    .cpuClkDiv = 2,
    .dmaClkDiv = 2,
    .mmioClkDiv = 2,
    .useStandbyClk = CyFalse,
    .clkSrc = CY_U3P_SYS_CLK,
};

static const CyU3PIoMatrixConfig_t s_IOConfig = {
    .isDQ32Bit = CyTrue,
    .useUart = CyFalse,
    .useI2C = CyTrue,
    .useI2S = CyFalse,
    .useSpi = CyFalse,
    .s0Mode = CY_U3P_SPORT_INACTIVE,
    .s1Mode = CY_U3P_SPORT_INACTIVE,
    .lppMode = CY_U3P_IO_MATRIX_LPP_DEFAULT,
    .gpioSimpleEn = {CY_FX_GPIO_TO_LOFLAG(CY_FX_GPIO_DDR_PWR_EN),
                     CY_FX_GPIO_TO_HIFLAG(CY_FX_GPIO_BIAS_EN) |
                         CY_FX_GPIO_TO_HIFLAG(CY_FX_GPIO_FPGA_PWR_EN) |
                         CY_FX_GPIO_TO_HIFLAG(CY_FX_GPIO_FPGA_PWR_INT) |
                         CY_FX_GPIO_TO_HIFLAG(CY_FX_GPIO_CDCE6214_PDN)},
    .gpioComplexEn = {0, 0},
};

int main() {
    CHECK_SUCCESS(CyU3PDeviceInit(const_cast<CyU3PSysClockConfig_t*>(&s_SysClockConfig)));

    /* Initialize the caches. Enable both Instruction and Data Caches. */
    CHECK_SUCCESS(CyU3PDeviceCacheControl(CyTrue, CyTrue, CyTrue));

    /* Configure the IO matrix for the device. */
    CHECK_SUCCESS(CyU3PDeviceConfigureIOMatrix(const_cast<CyU3PIoMatrixConfig_t*>(&s_IOConfig)));

    /* This is a non returnable call for initializing the RTOS kernel */
    CyU3PKernelEntry();

    /* Dummy return to make the compiler happy */
    return 0;

handle_fatal_error:
    /* Cannot recover from this error. */
    while (1)
        ;
}
