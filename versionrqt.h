#pragma once

#include <cyu3types.h>

constexpr uint8_t CY_FX_RQT_VERSION = 0xB0;

void HandleVersionRqt(uint8_t bReqType, uint16_t wValue, uint16_t wIndex, uint16_t wLength);
