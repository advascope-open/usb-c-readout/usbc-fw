#include "axirqt.h"
#include "dmachannels.h"

#include <cyu3error.h>
#include <cyu3usb.h>

#include "axi.h"
#include "usbep0.h"

extern uint8_t g_SlFifoBuffer[32];

void HandleAxiRqt(uint8_t bReqType, uint16_t wValue, uint16_t wIndex, uint16_t wLength) {
    if (wLength != sizeof(uint32_t)) {
        CyU3PUsbStall(0, CyTrue, CyFalse);
        return;
    }

    auto const address = ((uint32_t)wIndex << 16) | (uint32_t)wValue;
    auto* pdwEp0Buffer = reinterpret_cast<uint32_t*>(g_Ep0Buffer);
    auto& data = pdwEp0Buffer[0];

    if ((bReqType & 0x80) != 0) {
        g_AXILiteResultRd = AXILiteRead(address, data);
        if (g_AXILiteResultRd == 0) {
            CyU3PUsbSendEP0Data(wLength, g_Ep0Buffer);
            return;
        }
    } else {
        uint16_t len;
        auto ret = CyU3PUsbGetEP0Data(wLength, g_Ep0Buffer, &len);
        if (ret == CY_U3P_SUCCESS && len == wLength) {
            g_AXILiteResultWr = AXILiteWrite(address, data);
            return;
        }
    }

    CyU3PUsbStall(0, CyTrue, CyFalse);
}

void HandleAxiResultRqt(uint8_t bReqType, uint16_t wValue, uint16_t wIndex, uint16_t wLength) {
    if (wLength != sizeof(uint32_t) || (bReqType & 0x80) == 0) {
        CyU3PUsbStall(0, CyTrue, CyFalse);
        return;
    }

    auto* pdwEp0Buffer = reinterpret_cast<int*>(g_Ep0Buffer);
    auto const address = ((uint32_t)wIndex << 16) | (uint32_t)wValue;
    if (address == 0U) {
        pdwEp0Buffer[0] = g_AXILiteResultWr;
        CyU3PUsbSendEP0Data(wLength, g_Ep0Buffer);
    } else if (address == 1U) {
        pdwEp0Buffer[0] = g_AXILiteResultRd;
        CyU3PUsbSendEP0Data(wLength, g_Ep0Buffer);
    } else {
        CyU3PUsbStall(0, CyTrue, CyFalse);
    }
}
