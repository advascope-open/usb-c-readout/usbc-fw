#pragma once
#include <cyu3types.h>
#include <cyu3usb.h>

void UsbInit();
void UsbStart();
void UsbStop();
void UsbFlush();

auto UsbBulkEpPacketSize(CyU3PUSBSpeed_t) noexcept -> uint16_t;

extern auto UsbHandleVendorRqt(uint8_t bReqType,
                               uint8_t bRequest,
                               uint16_t wValue,
                               uint16_t wIndex,
                               uint16_t wLength) -> CyBool_t;

extern volatile uint32_t g_EndpointUnderrunCount;
