#include "appln.h"
#include "dma.h"
#include "gpif.h"
#include "gpio.h"
#include "iic.h"
#include "usb.h"

#include "axi.h"
#include "axirqt.h"
#include "versionrqt.h"
#include "xildma.h"
#include "xilspi.h"

#include <cyu3error.h>

CyU3PEvent g_ApplnEvent;
CyU3PThread g_ApplnThread;
volatile CyBool_t g_IsApplnActive = CyFalse;
volatile uint32_t g_setupdat0;
volatile uint32_t g_setupdat1;

/* This function starts the application. This is called
   when a SET_CONF event is received from the USB host. The endpoints
   are configured and the DMA pipe is setup in this function. */
void ApplnStart() {
    UsbStart();
    UsbFlush();
    GpifStart();
    DmaSlFifoStart();
    DmaUsbStart();

    g_IsApplnActive = CyTrue;
}

void ApplnStop() {
    g_IsApplnActive = CyFalse;

    UsbFlush();
    DmaUsbStop();
    DmaSlFifoStop();
    DmaI2cReset();
    GpifStop();
    UsbStop();
}

/* Entry function for the g_ApplnThread. */
static void Entry(uint32_t input) {
    UsbInit();
    GpioInit();
    I2cInit();
    DmaI2cStart();
    GpifInit();

    while (!g_IsApplnActive)
        CyU3PThreadSleep(100);

    CyU3PReturnStatus_t stat;
    uint32_t eventMask = CY_FX_USB_EP0_TASK | CY_FX_USB_OUTI_TASK;
    uint32_t eventStat;

    for (;;) {
        stat = CyU3PEventGet(&g_ApplnEvent, eventMask, CYU3P_EVENT_OR_CLEAR, &eventStat, 10);
        if (stat == CY_U3P_SUCCESS) {
            if (eventStat & CY_FX_USB_EP0_TASK) {
                uint8_t bRequest, bReqType;
                uint16_t wLength, wValue, wIndex;

                bReqType = (g_setupdat0 & CY_U3P_USB_REQUEST_TYPE_MASK);
                bRequest = ((g_setupdat0 & CY_U3P_USB_REQUEST_MASK) >> CY_U3P_USB_REQUEST_POS);
                wLength = ((g_setupdat1 & CY_U3P_USB_LENGTH_MASK) >> CY_U3P_USB_LENGTH_POS);
                wValue = ((g_setupdat0 & CY_U3P_USB_VALUE_MASK) >> CY_U3P_USB_VALUE_POS);
                wIndex = ((g_setupdat1 & CY_U3P_USB_INDEX_MASK) >> CY_U3P_USB_INDEX_POS);

                if ((bReqType & CY_U3P_USB_TYPE_MASK) == CY_U3P_USB_VENDOR_RQT) {
                    switch (bRequest) {
                        case CY_FX_RQT_VERSION:
                            HandleVersionRqt(bReqType, wValue, wIndex, wLength);
                            break;

                        case CY_FX_RQT_AXI:
                            HandleAxiRqt(bReqType, wValue, wIndex, wLength);
                            break;

                        case CY_FX_RQT_AXI_RESULT:
                            HandleAxiResultRqt(bReqType, wValue, wIndex, wLength);
                            break;

                        case CY_FX_RQT_XAXIDMA_INITIALIZE:
                            HandleXilDmaInitRqt(wIndex);
                            break;

                        case CY_FX_RQT_XAXIDMA_RESET:
                            HandleXilDmaResetRqt(wIndex);
                            break;

                        case CY_FX_RQT_XAXIDMA_MM2S:
                            HandleXilDmaToStreamRqt(bReqType, wValue, wIndex, wLength);
                            break;

                        case CY_FX_RQT_XAXIDMA_S2MM:
                            HandleXilDmaToMemoryRqt(bReqType, wValue, wIndex, wLength);
                            break;

                        case CY_FX_RQT_XSPI_INITIALIZE:
                            HandleXilSpiInitRqt(wIndex);
                            break;

                        case CY_FX_RQT_XSPI_RESET:
                            HandleXilSpiResetRqt(wIndex);
                            break;

                        case CY_FX_RQT_XSPI_SELFTEST:
                            HandleXilSpiSelfTestRqt(wIndex);
                            break;

                        case CY_FX_RQT_XSPI_STARTSTOP:
                            HandleXilSpiStartStopRqt(wValue, wIndex);
                            break;

                        case CY_FX_RQT_XSPI_STATS:
                            HandleXilSpiStatsRqt(bReqType, wValue, wIndex, wLength);
                            break;

                        case CY_FX_RQT_XSPI_TRANSFER:
                            HandleXilSpiTransferRqt(bReqType, wValue, wIndex, wLength);
                            break;

                        case CY_FX_RQT_I2C_TRANSFER:
                            HandleI2cTransferRqt(bReqType, wValue, wIndex, wLength);
                            break;

                        case CY_FX_RQT_I2C_TRANSFER_RESULT:
                            HandleI2cTransferResultRqt(bReqType, wValue, wIndex, wLength);
                            break;

                        case CY_FX_RQT_GPIO_GETVALUE:
                            HandleGpioGetValue(bReqType, wValue, wIndex, wLength);
                            break;

                        case CY_FX_RQT_GPIO_SETVALUE:
                            HandleGpioSetValue(bReqType, wValue, wIndex, wLength);
                            break;

                        default: /* Unknown request. Stall EP0. */
                            CyU3PUsbStall(0, CyTrue, CyFalse);
                            break;
                    }
                } else {
                    /* Only vendor requests are to be handled here. */
                    CyU3PUsbStall(0, CyTrue, CyFalse);
                }
            }

            if (eventStat & CY_FX_USB_OUTI_TASK) {
                AXIStreamToMemory();
            }
        }
    }
}

/* Application define function which creates the threads. */
void CyFxApplicationDefine() {
    constexpr UINT priority = 8;
    constexpr ULONG stack = 0x0400;

    void* ptr = nullptr;
    uint32_t ret = CY_U3P_SUCCESS;

    /* Create an event flag group that will be used for signalling the application thread. */
    ret = CyU3PEventCreate(&g_ApplnEvent);
    if (ret != 0)
        goto handle_fatal_error;

    /* Allocate the memory for the threads */
    ptr = CyU3PMemAlloc(stack);

    /* Create the thread for the application */
    ret = CyU3PThreadCreate(&g_ApplnThread, "21_APPLN_THREAD", &Entry, 0, ptr, stack, priority,
                            priority, CYU3P_NO_TIME_SLICE, CYU3P_AUTO_START);
    if (ret != 0)
        goto handle_fatal_error;

    return;

handle_fatal_error:
    /* Application cannot continue */
    /* Loop indefinitely */
    while (1)
        ;
}
