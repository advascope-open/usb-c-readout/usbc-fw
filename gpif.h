#pragma once

#include <cyu3types.h>

extern volatile uint32_t g_PibThread0AdapOverrun;
extern volatile uint32_t g_PibThread0AdapUnderrun;
extern volatile uint32_t g_PibThread0Direction;
extern volatile uint32_t g_PibThread0RdBurst;
extern volatile uint32_t g_PibThread0RdForceEnd;
extern volatile uint32_t g_PibThread0RdUnderrun;
extern volatile uint32_t g_PibThread0SckInactive;
extern volatile uint32_t g_PibThread0WrOverrun;
extern volatile uint32_t g_PibThread1AdapOverrun;
extern volatile uint32_t g_PibThread1AdapUnderrun;
extern volatile uint32_t g_PibThread1Direction;
extern volatile uint32_t g_PibThread1RdBurst;
extern volatile uint32_t g_PibThread1RdForceEnd;
extern volatile uint32_t g_PibThread1RdUnderrun;
extern volatile uint32_t g_PibThread1SckInactive;
extern volatile uint32_t g_PibThread1WrOverrun;
extern volatile uint32_t g_PibThread2AdapOverrun;
extern volatile uint32_t g_PibThread2AdapUnderrun;
extern volatile uint32_t g_PibThread2Direction;
extern volatile uint32_t g_PibThread2RdBurst;
extern volatile uint32_t g_PibThread2RdForceEnd;
extern volatile uint32_t g_PibThread2RdUnderrun;
extern volatile uint32_t g_PibThread2SckInactive;
extern volatile uint32_t g_PibThread2WrOverrun;
extern volatile uint32_t g_PibThread3AdapOverrun;
extern volatile uint32_t g_PibThread3AdapUnderrun;
extern volatile uint32_t g_PibThread3Direction;
extern volatile uint32_t g_PibThread3RdBurst;
extern volatile uint32_t g_PibThread3RdForceEnd;
extern volatile uint32_t g_PibThread3RdUnderrun;
extern volatile uint32_t g_PibThread3SckInactive;
extern volatile uint32_t g_PibThread3WrOverrun;

void GpifInit();
void GpifStart();
void GpifStop();
