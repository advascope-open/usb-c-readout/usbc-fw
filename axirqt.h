#pragma once

#include <cyu3types.h>

constexpr uint8_t CY_FX_RQT_AXI = 0xB1;
constexpr uint8_t CY_FX_RQT_AXI_RESULT = 0xB2;

void HandleAxiRqt(uint8_t bReqType, uint16_t wValue, uint16_t wIndex, uint16_t wLength);
void HandleAxiResultRqt(uint8_t bReqType, uint16_t wValue, uint16_t wIndex, uint16_t wLength);
