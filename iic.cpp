#include "iic.h"
#include "dmachannels.h"
#include "usbep0.h"

#include <cyu3dma.h>
#include <cyu3error.h>
#include <cyu3usb.h>
#include <cyu3utils.h>

volatile int g_I2cResultRd = 0;
volatile int g_I2cResultWr = 0;

#define RD_BIT 1
#define WR_BIT 0

#define CHECK_SUCCESS(f)                        \
    do {                                        \
        CyU3PReturnStatus_t apiRetStatus = (f); \
        if (apiRetStatus != CY_U3P_SUCCESS) {   \
            goto handle_fatal_error;            \
        }                                       \
    } while (0)

void I2cInterrupt(CyU3PI2cEvt_t, CyU3PI2cError_t);

static const CyU3PI2cConfig_t s_I2cConfig = {
    .bitRate = CY_FX_I2C_BITRATE,
    .isDma = CyTrue,
    .busTimeout = 0xFFFFFFFF,
    .dmaTimeout = 0xFFFF,
};

static void SetI2cResult(CyU3PReturnStatus_t r, volatile int& result) {
    if (r == CY_U3P_SUCCESS) {
        result = 0U;
    } else if (r == CY_U3P_ERROR_FAILURE) {
        CyU3PI2cError_t err;
        if ((r = CyU3PI2cGetErrorCode(&err)) == CY_U3P_SUCCESS) {
            result = static_cast<uint32_t>(err) | 0x100U;
        } else {
            result = static_cast<uint32_t>(-CY_U3P_ERROR_FAILURE);
        }
    } else {
        result = static_cast<uint32_t>(-r);
    }
}

inline void slave_address_wr(CyU3PI2cPreamble_t& preamble, uint8_t address) noexcept {
    preamble.buffer[preamble.length++] = (address & 0xfe) | WR_BIT;
}

inline void slave_address_rd(CyU3PI2cPreamble_t& preamble, uint8_t address) noexcept {
    preamble.buffer[preamble.length++] = (address & 0xfe) | RD_BIT;
}

inline void register_address(CyU3PI2cPreamble_t& preamble, uint16_t reg, bool is_16bit) noexcept {
    if (is_16bit)
        preamble.buffer[preamble.length++] = reg >> 8;
    preamble.buffer[preamble.length++] = reg & 0xff;
}

inline void repeated_start_condition(CyU3PI2cPreamble_t& preamble) noexcept {
    preamble.ctrlMask |= uint16_t{1} << (preamble.length - 1);
}

void I2cInit() {
    /* Initialize and configure the I2C master module. */
    CHECK_SUCCESS(CyU3PI2cInit());

    /* Start the I2C master block. */
    CHECK_SUCCESS(CyU3PI2cSetConfig(const_cast<CyU3PI2cConfig_t*>(&s_I2cConfig), &I2cInterrupt));

    return;

handle_fatal_error:
    /* Cannot recover from this error. */
    while (1)
        ;
}

void HandleI2cTransferRqt(uint8_t bReqType, uint16_t wValue, uint16_t wIndex, uint16_t wLength) {
    if (wLength > 128 || wIndex == 0) {
        CyU3PUsbStall(0, CyTrue, CyFalse);
        return;
    }

    CyU3PI2cPreamble_t preamble = {.buffer = {}, .length = 0, .ctrlMask = 0};
    CyU3PReturnStatus_t r = CY_U3P_SUCCESS;
    if ((wIndex & 0x01) == 0 && (bReqType & 0x80) == 0) {  // W
        slave_address_wr(preamble, wIndex & 0xff);
        if (wIndex & 0x100)  // REG
            register_address(preamble, wValue, wIndex & 0x200);

        if (wLength > 0) {
            r = CyU3PUsbGetEP0Data(wLength, g_Ep0Buffer, nullptr);
            if (r == CY_U3P_SUCCESS) {
                CyU3PDmaBuffer_t buf_p = {
                    .buffer = g_Ep0Buffer,
                    .count = wLength,
                    .size = 128,
                    .status = 0,
                };

                r = CyU3PDmaChannelSetupSendBuffer(&g_DmaHandleI2cTx, &buf_p);
                SetI2cResult(r, g_I2cResultWr);
                if (r != CY_U3P_SUCCESS) {
                    return;
                }

                r = CyU3PI2cSendCommand(&preamble, wLength, CyFalse);
                SetI2cResult(r, g_I2cResultWr);
                if (r != CY_U3P_SUCCESS) {
                    return;
                }

                r = CyU3PI2cWaitForBlockXfer(CyFalse);
                SetI2cResult(r, g_I2cResultWr);
                return;
            }
        } else {
            r = CyU3PI2cWaitForAck(&preamble, 0);
            SetI2cResult(r, g_I2cResultWr);
            if (r == CY_U3P_SUCCESS)
                CyU3PUsbAckSetup();
            else
                CyU3PUsbStall(0, CyTrue, CyFalse);
            return;
        }

    } else if ((wIndex & 0x01) != 0 && (bReqType & 0x80) != 0) {  // R
        slave_address_wr(preamble, wIndex & 0xff);
        if (wIndex & 0x100)  // REG
            register_address(preamble, wValue, wIndex & 0x200);
        repeated_start_condition(preamble);
        slave_address_rd(preamble, wIndex & 0xff);

        CyU3PDmaBuffer_t buf_p = {
            .buffer = g_Ep0Buffer,
            .count = wLength,
            .size = 128,
            .status = 0,
        };

        r = CyU3PI2cSendCommand(&preamble, wLength, CyTrue);
        SetI2cResult(r, g_I2cResultRd);
        if (r != CY_U3P_SUCCESS) {
            CyU3PUsbStall(0, CyTrue, CyFalse);
            return;
        }

        r = CyU3PDmaChannelSetupRecvBuffer(&g_DmaHandleI2cRx, &buf_p);
        SetI2cResult(r, g_I2cResultRd);
        if (r != CY_U3P_SUCCESS) {
            CyU3PUsbStall(0, CyTrue, CyFalse);
            return;
        }

        r = CyU3PI2cWaitForBlockXfer(CyTrue);
        SetI2cResult(r, g_I2cResultRd);
        if (r != CY_U3P_SUCCESS) {
            r = CyU3PDmaChannelSetWrapUp(&g_DmaHandleI2cRx);
            CyU3PUsbStall(0, CyTrue, CyFalse);
            return;
        }

        if (wLength < buf_p.size) {
            r = CyU3PDmaChannelSetWrapUp(&g_DmaHandleI2cRx);
            SetI2cResult(r, g_I2cResultRd);
            if (r != CY_U3P_SUCCESS) {
                CyU3PUsbStall(0, CyTrue, CyFalse);
                return;
            }
        }

        r = CyU3PUsbSendEP0Data(wLength, g_Ep0Buffer);
        if (r == CY_U3P_SUCCESS) {
            return;
        }
    }

    CyU3PUsbStall(0, CyTrue, CyFalse);
}

void HandleI2cTransferResultRqt(uint8_t bReqType,
                                uint16_t wValue,
                                uint16_t wIndex,
                                uint16_t wLength) {
    if (wLength != sizeof(uint32_t) || (bReqType & 0x80) == 0) {
        CyU3PUsbStall(0, CyTrue, CyFalse);
        return;
    }

    auto* pdwEp0Buffer = reinterpret_cast<int*>(g_Ep0Buffer);
    auto const address = ((uint32_t)wIndex << 16) | (uint32_t)wValue;
    if (address == 0U) {
        pdwEp0Buffer[0] = g_I2cResultWr;
        CyU3PUsbSendEP0Data(wLength, g_Ep0Buffer);
        CyU3PDmaChannelReset(&g_DmaHandleI2cTx);
    } else if (address == 1U) {
        pdwEp0Buffer[0] = g_I2cResultRd;
        CyU3PUsbSendEP0Data(wLength, g_Ep0Buffer);
        CyU3PDmaChannelReset(&g_DmaHandleI2cRx);
    } else {
        CyU3PUsbStall(0, CyTrue, CyFalse);
    }
}

void I2cInterrupt(CyU3PI2cEvt_t evt,    /**< Type of event that occured. */
                  CyU3PI2cError_t error /**< Specifies the actual error/status code when
                                             the event is of type CY_U3P_I2C_EVENT_ERROR. */
) {
    (void)evt;
    (void)error;
}
