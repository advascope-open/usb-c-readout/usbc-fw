#pragma once

#include <cyu3dma.h>
#include <cyu3error.h>

class DmaBuffer {
public:
    DmaBuffer(CyU3PDmaChannel& ch) : dma_(ch), dmaInfo_(), discardOnCleanup_(false) {}

    ~DmaBuffer() {
        if (discardOnCleanup_)
            Discard();
    }

    auto Get(uint32_t waitOption) -> int {
        CyU3PReturnStatus_t ret = CY_U3P_SUCCESS;

        if (discardOnCleanup_ && dmaInfo_.buffer != nullptr)
            ret = Discard();

        ret = CyU3PDmaChannelGetBuffer(&dma_, &dmaInfo_, waitOption);
        if (ret == CY_U3P_SUCCESS)
            discardOnCleanup_ = true;

        return ret;
    }

    auto Commit(uint16_t count) -> int {
        auto ret = CyU3PDmaChannelCommitBuffer(&dma_, count, 0U);
        if (ret == CY_U3P_SUCCESS)
            discardOnCleanup_ = false;

        return ret;
    }

    auto Discard() -> int {
        auto ret = CyU3PDmaChannelDiscardBuffer(&dma_);
        if (ret == CY_U3P_SUCCESS)
            discardOnCleanup_ = false;

        return ret;
    }

    auto Data() -> uint32_t* { return reinterpret_cast<uint32_t*>(dmaInfo_.buffer); }
    auto Count() -> uint16_t { return dmaInfo_.count; }
    auto Capacity() -> uint16_t { return dmaInfo_.size; }

private:
    CyU3PDmaChannel& dma_;
    CyU3PDmaBuffer_t dmaInfo_;
    bool discardOnCleanup_;
};
