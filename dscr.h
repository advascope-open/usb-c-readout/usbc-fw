#pragma once
#include <cyu3types.h>

extern uint8_t const USB20DeviceDscr[];
extern uint8_t const USB30DeviceDscr[];
extern uint8_t const USBDeviceQualDscr[];
extern uint8_t const USBFSConfigDscr[];
extern uint8_t const USBHSConfigDscr[];
extern uint8_t const USBBOSDscr[];
extern uint8_t const USBSSConfigDscr[];
extern uint8_t const USBStringLangIDDscr[];
extern uint8_t const USBManufactureDscr[];
extern uint8_t const USBProductDscr[];
