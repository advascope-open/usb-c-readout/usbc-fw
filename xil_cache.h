#ifndef XIL_CACHE_H
#define XIL_CACHE_H

#ifdef __cplusplus
extern "C" {
#endif

#define Xil_DCacheEnable()
#define Xil_DCacheDisable()
#define Xil_DCacheInvalidate()
#define Xil_DCacheInvalidateRange(Addr, Len)
#define Xil_DCacheFlush()
#define Xil_DCacheFlushRange(Addr, Len)
#define Xil_DCacheInvalidateLine(Addr)
#define Xil_DCacheFlushLine(Addr)

#define Xil_ICacheInvalidateLine(Addr)
#define Xil_ICacheEnable()
#define Xil_ICacheDisable()
#define Xil_ICacheInvalidate()
#define Xil_ICacheInvalidateRange(Addr, Len)

#define DATA_SYNC

#ifdef __cplusplus
}
#endif

#endif
