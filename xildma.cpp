#include "xildma.h"
#include "axi.h"

#include "dmachannels.h"
#include "usbep0.h"

#include <cyu3error.h>
#include <cyu3usb.h>
#include <cyu3utils.h>

#define XIL_IO_H
#include <xil_types.h>
#include <xaxidma.h>
#include <xaxidma_bd.h>
#include <xstatus.h>
#undef XAxiDma_ReadReg
#undef XAxiDma_WriteReg
#undef XAxiDma_BdRead
#undef XAxiDma_BdWrite

#include <csetjmp>

XAxiDma g_XAxiDma0;
XAxiDma g_XAxiDma1;
static jmp_buf s_JmpBuf;
extern volatile int g_AXILiteResultRd;
extern volatile int g_AXILiteResultWr;

constexpr UINTPTR TX_BD_SPACE_BASE = 0x81000000;
constexpr UINTPTR RX_BD_SPACE_BASE = 0x81001000;

/* extern */
u32 XAxiDma_ReadReg(u32 BaseAddress, u32 RegOffset) {
    uint32_t data;

    g_AXILiteResultRd = AXILiteRead(BaseAddress + RegOffset, data);
    if (g_AXILiteResultRd == 0)
        return data;
    else
        std::longjmp(s_JmpBuf, g_AXILiteResultRd);
}

/* extern */
void XAxiDma_WriteReg(u32 BaseAddress, u32 RegOffset, u32 Data) {
    g_AXILiteResultWr = AXILiteWrite(BaseAddress + RegOffset, Data);
    if (g_AXILiteResultWr == 0)
        return;
    else
        std::longjmp(s_JmpBuf, g_AXILiteResultWr);
}

/* extern */
u32 XAxiDma_BdRead(u32 BaseAddress, u32 Offset) {
    uint32_t data;

    g_AXILiteResultRd = AXILiteRead(BaseAddress + Offset, data);
    if (g_AXILiteResultRd == 0)
        return data;
    else
        std::longjmp(s_JmpBuf, g_AXILiteResultRd);
}

/* extern */
void XAxiDma_BdWrite(u32 BaseAddress, u32 Offset, u32 Data) {
    g_AXILiteResultWr = AXILiteWrite(BaseAddress + Offset, Data);
    if (g_AXILiteResultWr == 0)
        return;
    else
        std::longjmp(s_JmpBuf, g_AXILiteResultWr);
}

static auto GetAxiDmaInstance(uint16_t wIndex) -> XAxiDma* {
    if (wIndex == 0U)
        return &g_XAxiDma0;
    if (wIndex == 1U)
        return &g_XAxiDma1;
    else
        return nullptr;
}

static auto GetPageSizeForDmaToMem(uint16_t wIndex) -> uint32_t {
    if (wIndex == 0U)
        return (g_DmaHandleSlFifoCtoP.size - 31U) & ~31U;
    if (wIndex == 1U)
        return ~0;
    else
        return 0;
}

void HandleXilDmaInitRqt(uint16_t wIndex) {
    XAxiDma* instance = nullptr;
    XAxiDma_Config* cfg = nullptr;
    int ret = XST_SUCCESS;

    instance = GetAxiDmaInstance(wIndex);
    cfg = XAxiDma_LookupConfig(wIndex);
    if (cfg != nullptr && instance != nullptr) {
        if (setjmp(s_JmpBuf) == 0) {
            ret = XAxiDma_CfgInitialize(instance, cfg);
            if (ret == XST_SUCCESS) {
                XAxiDma_IntrDisable(instance, XAXIDMA_IRQ_ALL_MASK, XAXIDMA_DEVICE_TO_DMA);

                CyU3PUsbAckSetup();
                return;
            }
        }
    }

    CyU3PUsbStall(0, CyTrue, CyFalse);
}

void HandleXilDmaResetRqt(uint16_t wIndex) {
    XAxiDma* instance = nullptr;
    int ret = XST_SUCCESS;

    instance = GetAxiDmaInstance(wIndex);
    if (instance != nullptr) {
        if (setjmp(s_JmpBuf) == 0) {
            ret = XAxiDma_Selftest(instance);
            if (ret == XST_SUCCESS) {
                CyU3PUsbAckSetup();
                return;
            }
        }
    }

    CyU3PUsbStall(0, CyTrue, CyFalse);
}

void HandleXilDmaToStreamRqt(uint8_t bReqType, uint16_t wValue, uint16_t wIndex, uint16_t wLength) {
    if (wLength != 2 * sizeof(uint32_t) || (bReqType & 0x80) != 0) {
        CyU3PUsbStall(0, CyTrue, CyFalse);
        return;
    }

    XAxiDma* instance = GetAxiDmaInstance(wIndex);
    if (instance == nullptr) {
        CyU3PUsbStall(0, CyTrue, CyFalse);
        return;
    }

    UINTPTR TxChanBase = instance->TxBdRing.ChanBase;

    if (!(XAxiDma_ReadReg(TxChanBase, XAXIDMA_SR_OFFSET) & XAXIDMA_HALTED_MASK)) {
        if (XAxiDma_Busy(instance, XAXIDMA_DMA_TO_DEVICE))
            longjmp(s_JmpBuf, -CY_U3P_ERROR_TIMEOUT);
    }

    uint16_t len;
    auto ret = CyU3PUsbGetEP0Data(wLength, g_Ep0Buffer, &len);
    if (ret != CY_U3P_SUCCESS || len != wLength) {
        CyU3PUsbStall(0, CyTrue, CyFalse);
        return;
    }

    auto* pdwEp0Buffer = reinterpret_cast<uint32_t*>(g_Ep0Buffer);

    if (setjmp(s_JmpBuf) == 0) {
        auto const TX_BUFFER_BASE = pdwEp0Buffer[0];
        auto const MM2S_LEN = pdwEp0Buffer[1];

        UINTPTR TxBufDesc = TX_BD_SPACE_BASE;
        uint32_t const nBufDesc = 1UL;

        XAxiDma_BdWrite(TxBufDesc, XAXIDMA_BD_NDESC_OFFSET, TxBufDesc);
        XAxiDma_BdWrite(TxBufDesc, XAXIDMA_BD_NDESC_MSB_OFFSET, 0);
        XAxiDma_BdWrite(TxBufDesc, XAXIDMA_BD_BUFA_OFFSET, TX_BUFFER_BASE);
        XAxiDma_BdWrite(TxBufDesc, XAXIDMA_BD_BUFA_MSB_OFFSET, 0);

        XAxiDma_BdWrite(TxBufDesc, XAXIDMA_BD_MCCTL_OFFSET, 0);
        XAxiDma_BdWrite(TxBufDesc, XAXIDMA_BD_STRIDE_VSIZE_OFFSET, 0);

        XAxiDma_BdWrite(
            TxBufDesc, XAXIDMA_BD_CTRL_LEN_OFFSET,
            (MM2S_LEN & 0x03ffffff) | XAXIDMA_BD_CTRL_TXSOF_MASK | XAXIDMA_BD_CTRL_TXEOF_MASK);
        XAxiDma_BdWrite(TxBufDesc, XAXIDMA_BD_STS_OFFSET, 0);

        XAxiDma_BdWrite(TxBufDesc, XAXIDMA_BD_USR0_OFFSET, 0);
        XAxiDma_BdWrite(TxBufDesc, XAXIDMA_BD_USR1_OFFSET, 1);
        XAxiDma_BdWrite(TxBufDesc, XAXIDMA_BD_USR2_OFFSET, 2);
        XAxiDma_BdWrite(TxBufDesc, XAXIDMA_BD_USR3_OFFSET, 3);
        XAxiDma_BdWrite(TxBufDesc, XAXIDMA_BD_USR4_OFFSET, 4);

        XAXIDMA_CACHE_FLUSH(TxBufDesc);

        XAxiDma_IntrAckIrq(instance, XAXIDMA_IRQ_ALL_MASK, XAXIDMA_DMA_TO_DEVICE);

        XAxiDma_WriteReg(TxChanBase, XAXIDMA_CDESC_OFFSET,
                         TX_BD_SPACE_BASE & XAXIDMA_DESC_LSB_MASK);

        XAxiDma_WriteReg(TxChanBase, XAXIDMA_CR_OFFSET,
                         XAxiDma_ReadReg(TxChanBase, XAXIDMA_CR_OFFSET) | XAXIDMA_CR_RUNSTOP_MASK |
                             (1 << XAXIDMA_COALESCE_SHIFT) | (0 << XAXIDMA_DELAY_SHIFT));

        XAxiDma_WriteReg(TxChanBase, XAXIDMA_TDESC_OFFSET, TxBufDesc & XAXIDMA_DESC_LSB_MASK);
    }
}

void HandleXilDmaToMemoryRqt(uint8_t bReqType, uint16_t wValue, uint16_t wIndex, uint16_t wLength) {
    if (wLength != 2 * sizeof(uint32_t) || (bReqType & 0x80) != 0) {
        CyU3PUsbStall(0, CyTrue, CyFalse);
        return;
    }

    XAxiDma* instance = GetAxiDmaInstance(wIndex);
    if (instance == nullptr) {
        CyU3PUsbStall(0, CyTrue, CyFalse);
        return;
    }

    UINTPTR const RxChanBase = instance->RxBdRing[0].ChanBase;

    if (!(XAxiDma_ReadReg(RxChanBase, XAXIDMA_SR_OFFSET) & XAXIDMA_HALTED_MASK)) {
        if (XAxiDma_Busy(instance, XAXIDMA_DEVICE_TO_DMA)) {
            CyU3PUsbStall(0, CyTrue, CyFalse);
            return;
        }
    }

    uint16_t len;
    auto ret = CyU3PUsbGetEP0Data(wLength, g_Ep0Buffer, &len);
    if (ret != CY_U3P_SUCCESS || len != wLength) {
        CyU3PUsbStall(0, CyTrue, CyFalse);
        return;
    }

    auto* pdwEp0Buffer = reinterpret_cast<uint32_t*>(g_Ep0Buffer);

    if (setjmp(s_JmpBuf) == 0) {
        auto const RX_BUFFER_BASE = pdwEp0Buffer[0];
        auto const S2MM_LEN = pdwEp0Buffer[1];
        auto remaining = S2MM_LEN;

        UINTPTR RxBufDesc = RX_BD_SPACE_BASE;
        uint32_t const pageSize = GetPageSizeForDmaToMem(wIndex);
        uint32_t const nBufDesc = (S2MM_LEN / pageSize) + !!(S2MM_LEN % pageSize);

        AXIStreamToMemoryInitialize();

        for (auto i = 0U; i < nBufDesc; ++i) {
            uint32_t const bytesCount = CY_U3P_MIN(pageSize, remaining);

            UINTPTR RxNextBufDesc = RxBufDesc + XAXIDMA_BD_MINIMUM_ALIGNMENT;
            if (i == nBufDesc - 1U)
                RxNextBufDesc = RxBufDesc;

            XAxiDma_BdWrite(RxBufDesc, XAXIDMA_BD_NDESC_OFFSET, RxNextBufDesc);
            XAxiDma_BdWrite(RxBufDesc, XAXIDMA_BD_NDESC_MSB_OFFSET, 0);
            XAxiDma_BdWrite(RxBufDesc, XAXIDMA_BD_BUFA_OFFSET, RX_BUFFER_BASE + i * pageSize);
            XAxiDma_BdWrite(RxBufDesc, XAXIDMA_BD_BUFA_MSB_OFFSET, 0);

            XAxiDma_BdWrite(RxBufDesc, XAXIDMA_BD_MCCTL_OFFSET, 0);
            XAxiDma_BdWrite(RxBufDesc, XAXIDMA_BD_STRIDE_VSIZE_OFFSET, 0);

            XAxiDma_BdWrite(RxBufDesc, XAXIDMA_BD_CTRL_LEN_OFFSET,
                            (bytesCount & 0x03ffffff) | XAXIDMA_BD_CTRL_TXSOF_MASK |
                                XAXIDMA_BD_CTRL_TXEOF_MASK);
            XAxiDma_BdWrite(RxBufDesc, XAXIDMA_BD_STS_OFFSET, 0);

            XAxiDma_BdWrite(RxBufDesc, XAXIDMA_BD_USR0_OFFSET, 0);
            XAxiDma_BdWrite(RxBufDesc, XAXIDMA_BD_USR1_OFFSET, 1);
            XAxiDma_BdWrite(RxBufDesc, XAXIDMA_BD_USR2_OFFSET, 2);
            XAxiDma_BdWrite(RxBufDesc, XAXIDMA_BD_USR3_OFFSET, 3);
            XAxiDma_BdWrite(RxBufDesc, XAXIDMA_BD_USR4_OFFSET, 4);

            XAXIDMA_CACHE_FLUSH(RxBufDesc);

            RxBufDesc = RxNextBufDesc;
            remaining -= bytesCount;
        }

        XAxiDma_IntrAckIrq(instance, XAXIDMA_IRQ_ALL_MASK, XAXIDMA_DEVICE_TO_DMA);

        XAxiDma_WriteReg(RxChanBase, XAXIDMA_CDESC_OFFSET,
                         RX_BD_SPACE_BASE & XAXIDMA_DESC_LSB_MASK);

        XAxiDma_WriteReg(RxChanBase, XAXIDMA_CR_OFFSET,
                         XAxiDma_ReadReg(RxChanBase, XAXIDMA_CR_OFFSET) | XAXIDMA_CR_RUNSTOP_MASK |
                             (1 << XAXIDMA_COALESCE_SHIFT) | (0 << XAXIDMA_DELAY_SHIFT));

        XAxiDma_WriteReg(RxChanBase, XAXIDMA_TDESC_OFFSET, RxBufDesc & XAXIDMA_DESC_LSB_MASK);
    }
}
