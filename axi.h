#pragma once

#include <cyu3types.h>

extern volatile int g_AXILiteResultRd;
extern volatile int g_AXILiteResultWr;

auto AXILiteRead(uint32_t address, uint32_t& data) -> int;
auto AXILiteWrite(uint32_t address, uint32_t data) -> int;
void AXIStreamToMemoryInitialize();
void AXIStreamToMemory();
