#include "usb.h"
#include "usbep0.h"
#include "dmachannels.h"
#include "dscr.h"
#include "usbendpoints.h"

#include "appln.h"

#include <cyu3error.h>
#include <cyu3mmu.h>
#include <cyu3usb.h>
#include <cyu3utils.h>

#define CHECK_SUCCESS(f)                        \
    do {                                        \
        CyU3PReturnStatus_t apiRetStatus = (f); \
        if (apiRetStatus != CY_U3P_SUCCESS) {   \
            goto handle_fatal_error;            \
        }                                       \
    } while (0)

extern auto UsbSetupCallback(uint32_t, uint32_t) -> CyBool_t;
extern void UsbEventCallback(CyU3PUsbEventType_t, uint16_t);
extern auto UsbLPMRequestCallback(CyU3PUsbLinkPowerMode) -> CyBool_t;

alignas(1 << CYU3P_CACHE_LINE_SZ) uint8_t g_Ep0Buffer[4096];
volatile uint32_t g_EndpointUnderrunCount = 0;

static CyU3PEpConfig_t s_ProdEpConfig = {
    .enable = CyFalse,
    .epType = CY_U3P_USB_EP_BULK,
    .streams = 0,
    .pcktSize = 1024,
    .burstLen = CY_FX_EP_PRODUCER_BURST_LENGTH,
    .isoPkts = 0,
};

static CyU3PEpConfig_t s_ConsEpConfig = {
    .enable = CyFalse,
    .epType = CY_U3P_USB_EP_BULK,
    .streams = 0,
    .pcktSize = 1024,
    .burstLen = CY_FX_EP_CONSUMER_BURST_LENGTH,
    .isoPkts = 0,
};

void UsbInit() {
    /* Start the USB functionality. */
    CHECK_SUCCESS(CyU3PUsbStart());

    /* Register callbacks */
    CyU3PUsbRegisterSetupCallback(&UsbSetupCallback, CyTrue);
    CyU3PUsbRegisterEventCallback(&UsbEventCallback);
    CyU3PUsbRegisterLPMRequestCallback(&UsbLPMRequestCallback);

    /* Set the USB Enumeration descriptors */
    CHECK_SUCCESS(
        CyU3PUsbSetDesc(CY_U3P_USB_SET_SS_DEVICE_DESCR, 0, const_cast<uint8_t*>(USB30DeviceDscr)));
    CHECK_SUCCESS(
        CyU3PUsbSetDesc(CY_U3P_USB_SET_HS_DEVICE_DESCR, 0, const_cast<uint8_t*>(USB20DeviceDscr)));
    CHECK_SUCCESS(
        CyU3PUsbSetDesc(CY_U3P_USB_SET_SS_BOS_DESCR, 0, const_cast<uint8_t*>(USBBOSDscr)));
    CHECK_SUCCESS(
        CyU3PUsbSetDesc(CY_U3P_USB_SET_DEVQUAL_DESCR, 0, const_cast<uint8_t*>(USBDeviceQualDscr)));
    CHECK_SUCCESS(
        CyU3PUsbSetDesc(CY_U3P_USB_SET_SS_CONFIG_DESCR, 0, const_cast<uint8_t*>(USBSSConfigDscr)));
    CHECK_SUCCESS(
        CyU3PUsbSetDesc(CY_U3P_USB_SET_HS_CONFIG_DESCR, 0, const_cast<uint8_t*>(USBHSConfigDscr)));
    CHECK_SUCCESS(
        CyU3PUsbSetDesc(CY_U3P_USB_SET_FS_CONFIG_DESCR, 0, const_cast<uint8_t*>(USBFSConfigDscr)));
    CHECK_SUCCESS(
        CyU3PUsbSetDesc(CY_U3P_USB_SET_STRING_DESCR, 0, const_cast<uint8_t*>(USBStringLangIDDscr)));
    CHECK_SUCCESS(
        CyU3PUsbSetDesc(CY_U3P_USB_SET_STRING_DESCR, 1, const_cast<uint8_t*>(USBManufactureDscr)));
    CHECK_SUCCESS(
        CyU3PUsbSetDesc(CY_U3P_USB_SET_STRING_DESCR, 2, const_cast<uint8_t*>(USBProductDscr)));

    /* Connect the USB Pins with super speed operation enabled. */
    CHECK_SUCCESS(CyU3PConnectState(CyTrue, CyTrue));

    return;

handle_fatal_error:
    /* Cannot recover from this error. */
    while (1)
        ;
}

void UsbStart() {
    auto const usbSpeed = CyU3PUsbGetSpeed();

    /* Producer endpoint configuration */
    s_ProdEpConfig.enable = CyTrue;
    s_ProdEpConfig.pcktSize = UsbBulkEpPacketSize(usbSpeed);
    s_ProdEpConfig.burstLen = (usbSpeed == CY_U3P_SUPER_SPEED) ? CY_FX_EP_PRODUCER_BURST_LENGTH : 1;

    CHECK_SUCCESS(CyU3PSetEpConfig(CY_FX_EP_PRODUCER, &s_ProdEpConfig));

    /* Consumer endpoint configuration */
    s_ConsEpConfig.enable = CyTrue;
    s_ConsEpConfig.pcktSize = UsbBulkEpPacketSize(usbSpeed);
    s_ConsEpConfig.burstLen = (usbSpeed == CY_U3P_SUPER_SPEED) ? CY_FX_EP_CONSUMER_BURST_LENGTH : 1;

    CHECK_SUCCESS(CyU3PSetEpConfig(CY_FX_EP_CONSUMER, &s_ConsEpConfig));
    CHECK_SUCCESS(CyU3PUsbEPSetBurstMode(CY_FX_EP_CONSUMER, CyTrue));

    return;

handle_fatal_error:
    /* Cannot recover from this error. */
    while (1)
        ;
}

void UsbFlush() {
    /* Flush the endpoint memory */
    CHECK_SUCCESS(CyU3PUsbFlushEp(CY_FX_EP_PRODUCER));
    CHECK_SUCCESS(CyU3PUsbFlushEp(CY_FX_EP_CONSUMER));

    return;

handle_fatal_error:
    /* Cannot recover from this error. */
    while (1)
        ;
}

void UsbStop() {
    /* Disable producer endpoint. */
    s_ProdEpConfig.enable = CyFalse;
    CHECK_SUCCESS(CyU3PSetEpConfig(CY_FX_EP_PRODUCER, &s_ProdEpConfig));

    /* Disable consumer endpoint. */
    s_ConsEpConfig.enable = CyFalse;
    CHECK_SUCCESS(CyU3PSetEpConfig(CY_FX_EP_CONSUMER, &s_ConsEpConfig));

    return;

handle_fatal_error:
    /* Cannot recover from this error. */
    while (1)
        ;
}

auto UsbBulkEpPacketSize(CyU3PUSBSpeed_t const usbSpeed) noexcept -> uint16_t {
    static const uint16_t s_BulkPacketSize[] = {
        [CY_U3P_NOT_CONNECTED] = 0u,
        [CY_U3P_FULL_SPEED] = 64u,
        [CY_U3P_HIGH_SPEED] = 512u,
        [CY_U3P_SUPER_SPEED] = 1024u,
    };

    return s_BulkPacketSize[usbSpeed];
}

/* Callback to handle the USB setup requests. */
auto UsbSetupCallback(uint32_t const setupdat0, /**< Lower 4 bytes of the setup packet. */
                      uint32_t const setupdat1 /**< Upper 4 bytes of the setup packet. */)
    -> CyBool_t {
    CyBool_t isHandled = CyFalse;

    /* Decode the fields from the setup request. */
    uint8_t const bReqType = (setupdat0 & CY_U3P_USB_REQUEST_TYPE_MASK);
    uint8_t const bType = (bReqType & CY_U3P_USB_TYPE_MASK);
    uint8_t const bTarget = (bReqType & CY_U3P_USB_TARGET_MASK);
    uint8_t const bRequest = ((setupdat0 & CY_U3P_USB_REQUEST_MASK) >> CY_U3P_USB_REQUEST_POS);
    uint16_t const wValue = ((setupdat0 & CY_U3P_USB_VALUE_MASK) >> CY_U3P_USB_VALUE_POS);
    uint16_t const wIndex = ((setupdat1 & CY_U3P_USB_INDEX_MASK) >> CY_U3P_USB_INDEX_POS);

    if (bType == CY_U3P_USB_STANDARD_RQT) {
        /* Handle SET_FEATURE(FUNCTION_SUSPEND) and CLEAR_FEATURE(FUNCTION_SUSPEND)
         * requests here. It should be allowed to pass if the device is in configured
         * state and failed otherwise. */
        if ((bTarget == CY_U3P_USB_TARGET_INTF) &&
            ((bRequest == CY_U3P_USB_SC_SET_FEATURE) ||
             (bRequest == CY_U3P_USB_SC_CLEAR_FEATURE)) &&
            (wValue == 0)) {
            if (g_IsApplnActive)
                CyU3PUsbAckSetup();
            else
                CyU3PUsbStall(0, CyTrue, CyFalse);

            isHandled = CyTrue;
        }

        /* CLEAR_FEATURE request for endpoint is always passed to the setup callback
         * regardless of the enumeration model used. When a clear feature is received,
         * the previous transfer has to be flushed and cleaned up. This is done at the
         * protocol level. Since this is just a loopback operation, there is no higher
         * level protocol. So flush the EP memory and reset the DMA channel associated
         * with it. If there are more than one EP associated with the channel reset both
         * the EPs. The endpoint stall and toggle / sequence number is also expected to be
         * reset. Return CyFalse to make the library clear the stall and reset the endpoint
         * toggle. Or invoke the CyU3PUsbStall (ep, CyFalse, CyTrue) and return CyTrue.
         * Here we are clearing the stall. */
        if ((bTarget == CY_U3P_USB_TARGET_ENDPT) && (bRequest == CY_U3P_USB_SC_CLEAR_FEATURE) &&
            (wValue == CY_U3P_USBX_FS_EP_HALT)) {
            if (g_IsApplnActive) {
                if (wIndex == CY_FX_EP_PRODUCER) {
                    CyU3PUsbSetEpNak(CY_FX_EP_PRODUCER, CyTrue);
                    CyU3PBusyWait(125);

                    CyU3PDmaChannelReset(&g_DmaHandleSlFifoUtoC);
                    CyU3PUsbFlushEp(CY_FX_EP_PRODUCER);
                    CyU3PUsbResetEp(CY_FX_EP_PRODUCER);
                    CyU3PDmaChannelSetXfer(&g_DmaHandleSlFifoUtoC, 0U);

                    CyU3PUsbSetEpNak(CY_FX_EP_PRODUCER, CyFalse);
                }

                if (wIndex == CY_FX_EP_CONSUMER) {
                    CyU3PUsbSetEpNak(CY_FX_EP_CONSUMER, CyTrue);
                    CyU3PBusyWait(125);

                    CyU3PDmaMultiChannelReset(&g_DmaHandleSlFifoPtoU);
                    CyU3PUsbFlushEp(CY_FX_EP_CONSUMER);
                    CyU3PUsbResetEp(CY_FX_EP_CONSUMER);
                    CyU3PDmaMultiChannelSetXfer(&g_DmaHandleSlFifoPtoU, 0U, 0U);

                    CyU3PUsbSetEpNak(CY_FX_EP_CONSUMER, CyFalse);
                }

                CyU3PUsbStall(wIndex, CyFalse, CyTrue);

                CyU3PUsbAckSetup();
                isHandled = CyTrue;
            }
        }
    }

    /* Handle supported vendor requests. */
    if ((bType == CY_U3P_USB_VENDOR_RQT) && (bTarget == CY_U3P_USB_TARGET_DEVICE)) {
        /* We set an event here and let the application thread below handle these requests.
         * isHandled needs to be set to True, so that the driver does not stall EP0. */
        isHandled = CyTrue;
        g_setupdat0 = setupdat0;
        g_setupdat1 = setupdat1;
        CyU3PEventSet(&g_ApplnEvent, CY_FX_USB_EP0_TASK, CYU3P_EVENT_OR);
    }

    return isHandled;
}

/* This is the callback function to handle the USB events. */
void UsbEventCallback(CyU3PUsbEventType_t const evtype, /**< The event type. */
                      uint16_t const evdata             /**< Event specific data. */
) {
    switch (evtype) {
        default:
            break;

        case CY_U3P_USB_EVENT_CONNECT:
            break;

        case CY_U3P_USB_EVENT_SETCONF:
            /* If the application is already active
             * stop it before re-enabling. */
            if (g_IsApplnActive) {
                ApplnStop();
            }

            /* Start the function. */
            ApplnStart();
            break;

        case CY_U3P_USB_EVENT_RESET:
        case CY_U3P_USB_EVENT_DISCONNECT:
            /* Stop the function. */
            if (g_IsApplnActive) {
                ApplnStop();
            }
            break;

        case CY_U3P_USB_EVENT_EP_UNDERRUN:
            ++g_EndpointUnderrunCount;
            CyU3PUsbResetEndpointMemories();
            break;

        case CY_U3P_USB_EVENT_EP0_STAT_CPLT:
            break;
    }
}

/* Callback function to handle LPM requests from the USB 3.0 host. This function is invoked by
   the API whenever a state change from U0 -> U1 or U0 -> U2 happens. If we return CyTrue from
   this function, the FX3 device is retained in the low power state. If we return CyFalse, the
   FX3 device immediately tries to trigger an exit back to U0. */
auto UsbLPMRequestCallback(
    CyU3PUsbLinkPowerMode const link_mode /**< Link Power Mode requested by the USB 3.0 host. */)
    -> CyBool_t {
    return CyFalse;
}
