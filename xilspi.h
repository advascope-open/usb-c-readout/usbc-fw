#pragma once

#include <cyu3types.h>

constexpr uint8_t CY_FX_RQT_XSPI_INITIALIZE = 0xD0;
constexpr uint8_t CY_FX_RQT_XSPI_RESET = 0xD1;
constexpr uint8_t CY_FX_RQT_XSPI_SELFTEST = 0xD2;
constexpr uint8_t CY_FX_RQT_XSPI_STARTSTOP = 0xD3;
constexpr uint8_t CY_FX_RQT_XSPI_STATS = 0xD4;
constexpr uint8_t CY_FX_RQT_XSPI_TRANSFER = 0xD5;

typedef struct XSpi XSpi;
extern XSpi g_XSpi0;

void HandleXilSpiInitRqt(uint16_t wIndex);
void HandleXilSpiResetRqt(uint16_t wIndex);
void HandleXilSpiSelfTestRqt(uint16_t wIndex);
void HandleXilSpiStartStopRqt(uint16_t wValue, uint16_t wIndex);
void HandleXilSpiStatsRqt(uint8_t bReqType, uint16_t wValue, uint16_t wIndex, uint16_t wLength);
void HandleXilSpiTransferRqt(uint8_t bReqType, uint16_t wValue, uint16_t wIndex, uint16_t wLength);
