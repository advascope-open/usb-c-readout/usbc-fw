FX3FWROOT?=/Cypress/cyfx3sdk

all:compile

include $(FX3FWROOT)/fw_build/fx3_fw/fx3_build_config.mak

MODULE = cyfxreadout

SOURCE= $(MODULE).cpp \
	appln.cpp \
	axi.cpp \
	axirqt.cpp \
	cyfxcppsyscall.cpp \
	cyfxtx.cpp \
	dma.cpp \
	dscr.cpp \
	gpif.cpp \
	gpio.cpp \
	iic.cpp \
	usb.cpp \
	versionrqt.cpp \
	xildma.cpp \
	xilspi.cpp \
	Xilinx_embeddedsw/lib/bsp/standalone/src/common/xil_assert.c \
	Xilinx_embeddedsw/lib/bsp/standalone/src/common/xil_printf.c \
	$(wildcard Xilinx_embeddedsw/XilinxProcessorIPLib/drivers/axidma/src/*.c) \
	$(wildcard Xilinx_embeddedsw/XilinxProcessorIPLib/drivers/spi/src/*.c)

ifeq ($(CYFXBUILD),arm)
SOURCE_ASM=cyfx_startup.S
else
SOURCE_ASM=cyfx_gcc_startup.S
endif

USER_CFLAGS := -std=c++1y -fno-rtti -fno-exceptions \
    -fdata-sections -ffunction-sections \
    -Werror -Wno-unused-but-set-variable -Wno-unused-variable \
	-isystem Xilinx_embeddedsw/lib/bsp/standalone/src/common -I. -DXDEBUG_WARNING \
	-isystem Xilinx_embeddedsw/XilinxProcessorIPLib/drivers/axidma/src \
	-isystem Xilinx_embeddedsw/XilinxProcessorIPLib/drivers/spi/src

C_OBJECT=$(filter %.o,$(SOURCE:%.c=./%.o))
CPP_OBJECT=$(filter %.o,$(SOURCE:%.cpp=./%.o)) $(filter %.o,$(SOURCE:%.cc=./%.o))
A_OBJECT=$(SOURCE_ASM:%.S=./%.o)

$(filter ./Xilinx_embeddedsw/lib/bsp/standalone/src/common/%.o,$(C_OBJECT)): USER_CFLAGS += -fpermissive -Wno-error
$(filter ./Xilinx_embeddedsw/XilinxProcessorIPLib/drivers/axidma/src/%.o,$(C_OBJECT)): USER_CFLAGS += -fpermissive -Wno-error -include xparameters.h
$(filter ./Xilinx_embeddedsw/XilinxProcessorIPLib/drivers/spi/src/%.o,$(C_OBJECT)): USER_CFLAGS += -fpermissive -Wno-error -include xparameters.h

$(filter ./Xilinx_embeddedsw/XilinxProcessorIPLib/drivers/axidma/src/%.o,$(C_OBJECT)): xparameters.h
$(filter ./Xilinx_embeddedsw/XilinxProcessorIPLib/drivers/spi/src/%.o,$(C_OBJECT)): xparameters.h

EXES = $(MODULE).$(EXEEXT)

$(MODULE).$(EXEEXT): $(A_OBJECT) $(C_OBJECT) $(CPP_OBJECT)
	$(LINK)

cyfx_startup.S:
	cp $(FX3FWROOT)/fw_build/fx3_fw/cyfx_startup.S .

cyfx_gcc_startup.S:
	cp $(FX3FWROOT)/fw_build/fx3_fw/cyfx_gcc_startup.S .

cyfxtx.cpp:
	cp $(FX3FWROOT)/fw_build/fx3_fw/cyfxtx.cpp .

cyfxcppsyscall.cpp:
	cp $(FX3FWROOT)/fw_build/fx3_fw/cyfxcppsyscall.cpp .

%.o : %.cpp
	$(COMPILE)

%.o : %.cc
	$(COMPILE)

%.o : %.c
	$(COMPILE)

$(A_OBJECT) : %.o : %.S
	$(ASSEMBLE)

clean:
	rm -f ./$(MODULE).$(EXEEXT)
	rm -f ./$(MODULE).map
	rm -f ./*.o
	rm -f cyfxtx.cpp cyfxcppsyscall.cpp cyfx_startup.S cyfx_gcc_startup.S


compile: $(CPP_OBJECT) $(C_OBJECT) $(A_OBJECT) $(EXES)

.PHONY: clean compile

#[]#
