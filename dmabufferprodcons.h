#pragma once

#include <cyu3dma.h>
#include <cyu3error.h>
#include <cyu3utils.h>

class DmaBufferProdCons {
public:
    DmaBufferProdCons() noexcept = default;

    DmaBufferProdCons(CyU3PDmaChannel* producer_dma, CyU3PDmaChannel* consumer_dma) noexcept
        : prod_dma_(producer_dma), cons_dma_(consumer_dma), prod_dma_info_(), cons_dma_info_(){};

    void Process() {
        CyU3PReturnStatus_t r = CY_U3P_SUCCESS;

        while (!(IngressEmpty() && (r = IngressGet()) != CY_U3P_SUCCESS) &&
               !(EgressFull() && (r = EgressGet()) != CY_U3P_SUCCESS)) {
            if (!IngressEmpty())
                CopyData();

            if (EgressFull() || (IngressLastPacket() && !EgressEmpty())) {
                r = EgressCommit();
            } else if (EgressEmpty()) {
                r = EgressDiscard();
            }

            if (IngressEmpty()) {
                r = IngressDiscard();
            }
        }
    }

private:
    auto IngressGet() -> CyU3PReturnStatus_t {
        CyU3PReturnStatus_t r = CY_U3P_SUCCESS;
        r = CyU3PDmaChannelGetBuffer(prod_dma_, &prod_dma_info_, CYU3P_NO_WAIT);
        return r;
    }

    auto IngressAvailable() -> uint16_t { return prod_dma_info_.count; }

    auto IngressEmpty() -> bool { return IngressAvailable() == 0U; }

    auto IngressData() -> uint32_t* { return reinterpret_cast<uint32_t*>(prod_dma_info_.buffer); }

    auto IngressLastPacket() -> bool {
        return (prod_dma_info_.status & CY_U3P_DMA_BUFFER_EOP) != 0;
    }

    auto IngressDiscard() -> CyU3PReturnStatus_t {
        CyU3PReturnStatus_t r = CY_U3P_SUCCESS;
        r = CyU3PDmaChannelDiscardBuffer(prod_dma_);
        if (r == CY_U3P_SUCCESS)
            prod_dma_info_ = {};

        return r;
    }

    auto EgressData() -> uint32_t* {
        return reinterpret_cast<uint32_t*>(cons_dma_info_.buffer + cons_dma_info_.count);
    }

    auto EgressGet() -> CyU3PReturnStatus_t {
        CyU3PReturnStatus_t r = CY_U3P_SUCCESS;
        r = CyU3PDmaChannelGetBuffer(cons_dma_, &cons_dma_info_, 15U);
        if (r == CY_U3P_SUCCESS)
            cons_dma_info_.count = sizeof(uint32_t);

        return r;
    }

    auto EgressCommit() -> CyU3PReturnStatus_t {
        CyU3PReturnStatus_t r = CY_U3P_SUCCESS;
        reinterpret_cast<uint32_t*>(cons_dma_info_.buffer)[0] = __builtin_bswap32(
            (0b1001U << 28) | (cons_dma_info_.count - sizeof(uint32_t)) / sizeof(uint32_t));

        r = CyU3PDmaChannelCommitBuffer(cons_dma_, cons_dma_info_.count,
                                        CY_U3P_DMA_BUFFER_OCCUPIED | CY_U3P_DMA_BUFFER_EOP);
        if (r == CY_U3P_SUCCESS)
            cons_dma_info_ = {};

        return r;
    }

    auto EgressDiscard() -> CyU3PReturnStatus_t {
        CyU3PReturnStatus_t r = CY_U3P_SUCCESS;
        r = CyU3PDmaChannelDiscardBuffer(cons_dma_);
        if (r == CY_U3P_SUCCESS)
            cons_dma_info_ = {};

        return r;
    }

    auto EgressFree() -> uint16_t { return ((cons_dma_info_.size - cons_dma_info_.count) & ~31U); }

    auto EgressEmpty() -> bool { return cons_dma_info_.count <= sizeof(uint32_t); }
    auto EgressFull() -> bool { return EgressFree() == 0; }

    void CopyData() {
        uint16_t chunk = CY_U3P_MIN(EgressFree(), IngressAvailable());
        uint32_t dwCount = chunk / sizeof(uint32_t);
        CyU3PMemCopy32(EgressData(), IngressData(), dwCount);

        cons_dma_info_.count += chunk;
        prod_dma_info_.count -= chunk;
        prod_dma_info_.buffer += chunk;
    }

private:
    CyU3PDmaChannel* prod_dma_;
    CyU3PDmaChannel* cons_dma_;
    CyU3PDmaBuffer_t prod_dma_info_;
    CyU3PDmaBuffer_t cons_dma_info_;
};
