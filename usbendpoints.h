#pragma once
#include <cyu3types.h>

constexpr uint8_t CY_FX_EP_PRODUCER = 0x01; /* EP 1 OUT */
constexpr uint8_t CY_FX_EP_CONSUMER = 0x81; /* EP 1 IN */

constexpr uint8_t CY_FX_EP_PRODUCER_BURST_LENGTH = 4;
constexpr uint8_t CY_FX_EP_CONSUMER_BURST_LENGTH = 12;
