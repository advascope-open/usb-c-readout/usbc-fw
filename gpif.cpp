#include "gpif.h"
#include "cyfxgpif_syncsf.h"
#include "dmasockets.h"

#include <cyu3error.h>
#include <cyu3pib.h>

#define CHECK_SUCCESS(f)                        \
    do {                                        \
        CyU3PReturnStatus_t apiRetStatus = (f); \
        if (apiRetStatus != CY_U3P_SUCCESS) {   \
            goto handle_fatal_error;            \
        }                                       \
    } while (0)

static void GpifStateMachineInterrupt(uint8_t);
static void GpifEvent(CyU3PGpifEventType, uint8_t);
static void PibInterrupt(CyU3PPibIntrType, uint16_t);

static const CyU3PPibClock_t s_PibClockConfig = {
    .clkDiv = 4,
    .isHalfDiv = CyFalse,
    .isDllEnable = CyTrue,
    .clkSrc = CY_U3P_SYS_CLK,
};

constexpr uint16_t s_PibDllSlaveDelay = 0;
constexpr CyU3PPibClockPhase_t s_PibDllCorePhase = CyU3PPibClockPhase_t(1);
constexpr CyU3PPibClockPhase_t s_PibDllSyncPhase = CyU3PPibClockPhase_t(0);
constexpr CyU3PPibClockPhase_t s_PibDllOutputPhase = CyU3PPibClockPhase_t(0b1011);

void GpifInit() {
    /* Register callbacks */
    CyU3PGpifRegisterCallback(&GpifEvent);
    CyU3PGpifRegisterSMIntrCallback(&GpifStateMachineInterrupt);
    CyU3PPibRegisterCallback(
        &PibInterrupt, CYU3P_PIB_INTR_DLL_UPDATE | CYU3P_PIB_INTR_PPCONFIG | CYU3P_PIB_INTR_ERROR);

    return;
}

void GpifStart() {
    /* Initialize the p-port block. */
    CHECK_SUCCESS(CyU3PPibInit(CyTrue, const_cast<CyU3PPibClock_t*>(&s_PibClockConfig)));

    CHECK_SUCCESS(CyU3PSetPportDriveStrength(CY_U3P_DS_FULL_STRENGTH));

    CHECK_SUCCESS(CyU3PPibDllConfigure(CYU3P_PIB_DLL_MASTER, s_PibDllSlaveDelay, s_PibDllCorePhase,
                                       s_PibDllSyncPhase, s_PibDllOutputPhase, CyTrue));

    /* Load the GPIF configuration for Slave FIFO sync mode. */
    CHECK_SUCCESS(CyU3PGpifLoad(&CyFxGpifConfig));

    /* Configure the functionality of a GPIF output pin. */
    CHECK_SUCCESS(
        CyU3PGpifOutputConfigure(/* FLAGA: GPIO_21 */ 4, CYU3P_GPIF_OP_THR0_READY, CyTrue));
    CHECK_SUCCESS(
        CyU3PGpifOutputConfigure(/* FLAGB: GPIO_22 */ 5, CYU3P_GPIF_OP_THR2_READY, CyTrue));
    CHECK_SUCCESS(
        CyU3PGpifOutputConfigure(/* FLAGC: GPIO_23 */ 6, CYU3P_GPIF_OP_THR3_READY, CyTrue));
    CHECK_SUCCESS(CyU3PGpifOutputConfigure(/* FLAGD: INT# */ 15, CYU3P_GPIF_OP_PARTIAL, CyTrue));

    CHECK_SUCCESS(
        CyU3PGpifSocketConfigure(2, CY_FX_PRODUCER_DUAL_PPORT_SOCKET_FIRST, 6, CyFalse, 3));
    CHECK_SUCCESS(
        CyU3PGpifSocketConfigure(3, CY_FX_PRODUCER_DUAL_PPORT_SOCKET_SECOND, 6, CyFalse, 3));

    CHECK_SUCCESS(CyU3PGpifSMStart(RESET, ALPHA_RESET));

    return;

handle_fatal_error:
    /* Cannot recover from this error. */
    while (1)
        ;
}

void GpifStop() {
    CyU3PGpifDisable(CyTrue);
    CyU3PPibDeInit();
}

void GpifStateMachineInterrupt(
    uint8_t const stateId /**< Current state of the GPIF state machine. */) {
    return;
}

void GpifEvent(CyU3PGpifEventType const event, /**< Event type that is being notified. */
               uint8_t const currentState      /**< Current state of the State Machine. */
) {
    return;
}

void PibInterrupt(
    CyU3PPibIntrType const cbType, /**< Type of interrupt that caused this callback. */
    uint16_t const cbArg           /**< 16-bit integer argument associated with the interrupt. */
) {
    if (cbType == CYU3P_PIB_INTR_ERROR) {
        switch (CYU3P_GET_PIB_ERROR_TYPE(cbArg)) {
            case CYU3P_PIB_ERR_NONE:
                return;
            case CYU3P_PIB_ERR_THR0_DIRECTION:
                ++g_PibThread0Direction;
                break;
            case CYU3P_PIB_ERR_THR1_DIRECTION:
                ++g_PibThread1Direction;
                break;
            case CYU3P_PIB_ERR_THR2_DIRECTION:
                ++g_PibThread2Direction;
                break;
            case CYU3P_PIB_ERR_THR3_DIRECTION:
                ++g_PibThread3Direction;
                break;
            case CYU3P_PIB_ERR_THR0_WR_OVERRUN:
                ++g_PibThread0WrOverrun;
                break;
            case CYU3P_PIB_ERR_THR1_WR_OVERRUN:
                ++g_PibThread1WrOverrun;
                break;
            case CYU3P_PIB_ERR_THR2_WR_OVERRUN:
                ++g_PibThread2WrOverrun;
                break;
            case CYU3P_PIB_ERR_THR3_WR_OVERRUN:
                ++g_PibThread3WrOverrun;
                break;
            case CYU3P_PIB_ERR_THR0_RD_UNDERRUN:
                ++g_PibThread0RdUnderrun;
                break;
            case CYU3P_PIB_ERR_THR1_RD_UNDERRUN:
                ++g_PibThread1RdUnderrun;
                break;
            case CYU3P_PIB_ERR_THR2_RD_UNDERRUN:
                ++g_PibThread2RdUnderrun;
                break;
            case CYU3P_PIB_ERR_THR3_RD_UNDERRUN:
                ++g_PibThread3RdUnderrun;
                break;
            case CYU3P_PIB_ERR_THR0_SCK_INACTIVE:
                ++g_PibThread0SckInactive;
                break;
            case CYU3P_PIB_ERR_THR0_ADAP_OVERRUN:
                ++g_PibThread0AdapOverrun;
                break;
            case CYU3P_PIB_ERR_THR0_ADAP_UNDERRUN:
                ++g_PibThread0AdapUnderrun;
                break;
            case CYU3P_PIB_ERR_THR0_RD_FORCE_END:
                ++g_PibThread0RdForceEnd;
                break;
            case CYU3P_PIB_ERR_THR0_RD_BURST:
                ++g_PibThread0RdBurst;
                break;
            case CYU3P_PIB_ERR_THR1_SCK_INACTIVE:
                ++g_PibThread1SckInactive;
                break;
            case CYU3P_PIB_ERR_THR1_ADAP_OVERRUN:
                ++g_PibThread1AdapOverrun;
                break;
            case CYU3P_PIB_ERR_THR1_ADAP_UNDERRUN:
                ++g_PibThread1AdapUnderrun;
                break;
            case CYU3P_PIB_ERR_THR1_RD_FORCE_END:
                ++g_PibThread1RdForceEnd;
                break;
            case CYU3P_PIB_ERR_THR1_RD_BURST:
                ++g_PibThread1RdBurst;
                break;
            case CYU3P_PIB_ERR_THR2_SCK_INACTIVE:
                ++g_PibThread2SckInactive;
                break;
            case CYU3P_PIB_ERR_THR2_ADAP_OVERRUN:
                ++g_PibThread2AdapOverrun;
                break;
            case CYU3P_PIB_ERR_THR2_ADAP_UNDERRUN:
                ++g_PibThread2AdapUnderrun;
                break;
            case CYU3P_PIB_ERR_THR2_RD_FORCE_END:
                ++g_PibThread2RdForceEnd;
                break;
            case CYU3P_PIB_ERR_THR2_RD_BURST:
                ++g_PibThread2RdBurst;
                break;
            case CYU3P_PIB_ERR_THR3_SCK_INACTIVE:
                ++g_PibThread3SckInactive;
                break;
            case CYU3P_PIB_ERR_THR3_ADAP_OVERRUN:
                ++g_PibThread3AdapOverrun;
                break;
            case CYU3P_PIB_ERR_THR3_ADAP_UNDERRUN:
                ++g_PibThread3AdapUnderrun;
                break;
            case CYU3P_PIB_ERR_THR3_RD_FORCE_END:
                ++g_PibThread3RdForceEnd;
                break;
            case CYU3P_PIB_ERR_THR3_RD_BURST:
                ++g_PibThread3RdBurst;
                break;
        }
    }
}

volatile uint32_t g_PibThread0AdapOverrun = 0u;
volatile uint32_t g_PibThread0AdapUnderrun = 0u;
volatile uint32_t g_PibThread0Direction = 0u;
volatile uint32_t g_PibThread0RdBurst = 0u;
volatile uint32_t g_PibThread0RdForceEnd = 0u;
volatile uint32_t g_PibThread0RdUnderrun = 0u;
volatile uint32_t g_PibThread0SckInactive = 0u;
volatile uint32_t g_PibThread0WrOverrun = 0u;
volatile uint32_t g_PibThread1AdapOverrun = 0u;
volatile uint32_t g_PibThread1AdapUnderrun = 0u;
volatile uint32_t g_PibThread1Direction = 0u;
volatile uint32_t g_PibThread1RdBurst = 0u;
volatile uint32_t g_PibThread1RdForceEnd = 0u;
volatile uint32_t g_PibThread1RdUnderrun = 0u;
volatile uint32_t g_PibThread1SckInactive = 0u;
volatile uint32_t g_PibThread1WrOverrun = 0u;
volatile uint32_t g_PibThread2AdapOverrun = 0u;
volatile uint32_t g_PibThread2AdapUnderrun = 0u;
volatile uint32_t g_PibThread2Direction = 0u;
volatile uint32_t g_PibThread2RdBurst = 0u;
volatile uint32_t g_PibThread2RdForceEnd = 0u;
volatile uint32_t g_PibThread2RdUnderrun = 0u;
volatile uint32_t g_PibThread2SckInactive = 0u;
volatile uint32_t g_PibThread2WrOverrun = 0u;
volatile uint32_t g_PibThread3AdapOverrun = 0u;
volatile uint32_t g_PibThread3AdapUnderrun = 0u;
volatile uint32_t g_PibThread3Direction = 0u;
volatile uint32_t g_PibThread3RdBurst = 0u;
volatile uint32_t g_PibThread3RdForceEnd = 0u;
volatile uint32_t g_PibThread3RdUnderrun = 0u;
volatile uint32_t g_PibThread3SckInactive = 0u;
volatile uint32_t g_PibThread3WrOverrun = 0u;
