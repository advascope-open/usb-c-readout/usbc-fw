#include "axi.h"
#include "dmabuffer.h"
#include "dmabufferprodcons.h"
#include "dmabufferrecv.h"
#include "dmachannels.h"

#include <cyu3error.h>
#include <cyu3mmu.h>
#include <cyu3utils.h>

alignas(1 << CYU3P_CACHE_LINE_SZ) uint8_t g_SlFifoBuffer[32];
volatile int g_AXILiteResultRd = 0;
volatile int g_AXILiteResultWr = 0;

DmaBufferProdCons g_SlFifoProdCons;

auto AXILiteRead(uint32_t address, uint32_t& data) -> int {
    CyU3PReturnStatus_t ret = CY_U3P_SUCCESS;

    DmaBufferRecv recvBuf(g_DmaHandleSlFifoPtoC);
    if ((ret = recvBuf.Setup(g_SlFifoBuffer, sizeof(g_SlFifoBuffer))) != CY_U3P_SUCCESS)
        return -ret;

    DmaBuffer sendBuf(g_DmaHandleSlFifoCtoP);
    if ((ret = sendBuf.Get(15U)) != CY_U3P_SUCCESS)
        return -ret;

    auto* pdwBuf = sendBuf.Data();
    if (sendBuf.Capacity() < 2 * sizeof(*pdwBuf))
        return -CY_U3P_ERROR_BAD_SIZE;

    pdwBuf[0] = __builtin_bswap32((0b0100U << 28) | 1U);
    pdwBuf[1] = __builtin_bswap32(address);

    if ((ret = sendBuf.Commit(2 * sizeof(*pdwBuf))) != CY_U3P_SUCCESS)
        return -ret;

    if ((ret = recvBuf.WaitFor(100U)) != CY_U3P_SUCCESS)
        return -ret;

    pdwBuf = recvBuf.Data();
    if (recvBuf.Count() < 1 * sizeof(*pdwBuf))
        return -CY_U3P_ERROR_BAD_SIZE;

    auto const slFifoResult = __builtin_bswap32(pdwBuf[0]);
    if (slFifoResult == 0U) {
        if (recvBuf.Count() < 2 * sizeof(*pdwBuf))
            return -CY_U3P_ERROR_BAD_SIZE;

        data = __builtin_bswap32(pdwBuf[1]);
    }

    return static_cast<int>(slFifoResult);
}

auto AXILiteWrite(uint32_t address, uint32_t data) -> int {
    CyU3PReturnStatus_t ret = CY_U3P_SUCCESS;

    DmaBufferRecv recvBuf(g_DmaHandleSlFifoPtoC);
    if ((ret = recvBuf.Setup(g_SlFifoBuffer, sizeof(g_SlFifoBuffer))) != CY_U3P_SUCCESS)
        return -ret;

    DmaBuffer sendBuf(g_DmaHandleSlFifoCtoP);
    if ((ret = sendBuf.Get(15U)) != CY_U3P_SUCCESS)
        return -ret;

    auto* pdwBuf = sendBuf.Data();
    if (sendBuf.Capacity() < 3 * sizeof(*pdwBuf))
        return -CY_U3P_ERROR_BAD_SIZE;

    pdwBuf[0] = __builtin_bswap32((0b1100U << 28) | 2U);
    pdwBuf[1] = __builtin_bswap32(address);
    pdwBuf[2] = __builtin_bswap32(data);

    if ((ret = sendBuf.Commit(3 * sizeof(*pdwBuf))) != CY_U3P_SUCCESS)
        return -ret;

    if ((ret = recvBuf.WaitFor(100U)) != CY_U3P_SUCCESS)
        return -ret;

    pdwBuf = recvBuf.Data();
    if (recvBuf.Count() < 1 * sizeof(*pdwBuf))
        return -CY_U3P_ERROR_BAD_SIZE;

    auto const slFifoResult = __builtin_bswap32(pdwBuf[0]);
    return static_cast<int>(slFifoResult);
}

void AXIStreamToMemoryInitialize() {
    g_SlFifoProdCons = {&g_DmaHandleSlFifoUtoC, &g_DmaHandleSlFifoCtoP};
}

void AXIStreamToMemory() {
    g_SlFifoProdCons.Process();
}
