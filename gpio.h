#pragma once
#include <cyu3types.h>

constexpr uint8_t CY_FX_GPIO_DDR_PWR_EN = 26;
constexpr uint8_t CY_FX_GPIO_BIAS_EN = 45;
constexpr uint8_t CY_FX_GPIO_FPGA_PWR_EN = 56;
constexpr uint8_t CY_FX_GPIO_FPGA_PWR_INT = 57;
constexpr uint8_t CY_FX_GPIO_CDCE6214_PDN = 60;

constexpr uint8_t CY_FX_RQT_GPIO_GETVALUE = 0x90;
constexpr uint8_t CY_FX_RQT_GPIO_SETVALUE = 0x91;

extern void GpioInit();

void HandleGpioGetValue(uint8_t bReqType, uint16_t wValue, uint16_t wIndex, uint16_t wLength);
void HandleGpioSetValue(uint8_t bReqType, uint16_t wValue, uint16_t wIndex, uint16_t wLength);
